void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);

  // Initial prompt
  Serial.println("Please type some text and hit enter: ");
}

void loop() {
  // put your main code here, to run repeatedly:
  if (Serial.available()) {   
    // Read the characters available via serial as a string object
    String inputString = Serial.readString();

    // Echo back what you just said
    Serial.println("You said: " + inputString); 
  }
}
