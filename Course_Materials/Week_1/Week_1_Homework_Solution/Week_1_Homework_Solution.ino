void setup() {
  // Initialize LED pin
  pinMode(LED_BUILTIN, OUTPUT);

  // Initialize serial
  Serial.begin(9600);

  // Initial prompt
  Serial.println("Please type an integer and hit enter: ");
}

void loop() {
  // put your main code here, to run repeatedly:
  if (Serial.available()) {   
    // Read the characters available via serial as a string object
    int inputInt = Serial.parseInt();

    // Turn the LED on
    digitalWrite(LED_BUILTIN, HIGH);

    // Wait for number of seconds (convert seconds to ms)
    delay(inputInt * 1000);

    // Turn the LED off
    digitalWrite(LED_BUILTIN, LOW);
  }
}
