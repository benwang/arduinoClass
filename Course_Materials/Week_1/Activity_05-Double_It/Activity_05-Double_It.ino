void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);

  // Initial prompt
  Serial.println("Please type an integer and hit enter: ");
}

void loop() {
  // put your main code here, to run repeatedly:
  if (Serial.available()) {   
    // Read the characters available via serial as a string object
    int inputInt = Serial.parseInt();

    // Echo back what you just said
    Serial.println(String(inputInt) + " times two is " + String(inputInt * 2));
  }
}
