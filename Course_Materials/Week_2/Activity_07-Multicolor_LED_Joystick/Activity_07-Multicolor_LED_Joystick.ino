// Include math library for trig functions
#include <math.h>


// Pin definitions here
// These MUST be PWM pins. For Arduino UNO, choose from: 3, 5, 6, 9, 10, 11
const int RED_PIN = 6;
const int GREEN_PIN = 5;
const int BLUE_PIN = 3;

// Joystick pins
const int JOYSTICK_VX_PIN = A0;
const int JOYSTICK_VY_PIN = A1;

// Declaring RGB as global variables now so they can be accessed by colorWheelTransform()
int redValue = 0;
int greenValue = 0;
int blueValue = 0;

// Declare this before we use it
inline double normalizeAngle(double angle);

// Angle definitions for color wheel. These are the angles at which
// each color is off
const double RED_OFF_ANGLE = normalizeAngle(- M_PI / 2);
const double GREEN_OFF_ANGLE = normalizeAngle(RED_OFF_ANGLE + (2 * M_PI / 3));
const double BLUE_OFF_ANGLE = normalizeAngle(RED_OFF_ANGLE - (2 * M_PI / 3));

// Magnitude of joystick "deadzone"
const double JOYSTICK_DEADZONE_MAG = 0.07;

// ********************************************
// ************* HELPER FUNCTIONS *************
// ********************************************

// Function to normalize an angle to be between -pi and +pi
inline double normalizeAngle(double angle) {
  while (angle > M_PI) {
    angle -= 2 * M_PI;
  }
  while (angle < - M_PI) {
    angle += 2 * M_PI;
  }

  return angle;
}

// Given a joystick X,Y, convert it to an RGB value
void colorWheelTransform(int x, int y) {
  // Normalize and convert to floating point (-1.0 to 1.0, instead of 0 to 1023)
  double normX = (x - 512) / 512.0;
  double normY = (y - 512) / 512.0;
  
  Serial.print("norm: (" + String(normX) + ", " + String(normY) + ")  ");

  // Now compute the angle using trigonometry
  double angle = atan2(normY, normX);
  double magnitude = min(sqrt(normX * normX + normY * normY), 1.0);

  Serial.print("Angle: " + String(angle * 180 / M_PI) + " Mag: " + String(magnitude) + "  ");

  // Add a deadzone so that noise when stick is untouched doesn't jump around
  if (magnitude > JOYSTICK_DEADZONE_MAG) {
    // Serial.print(" R_OFF:" + String(RED_OFF_ANGLE) + " G_OFF:" + String(GREEN_OFF_ANGLE) + " B_OFF:" + String(BLUE_OFF_ANGLE) + "  ");
    
    // Blend the 3 components based on angle and magnitude
    redValue = 255 * magnitude * abs(normalizeAngle(RED_OFF_ANGLE - angle)) / M_PI;
    greenValue = 255 *  magnitude * abs(normalizeAngle(GREEN_OFF_ANGLE - angle)) / M_PI;
    blueValue = 255 * magnitude * abs(normalizeAngle(BLUE_OFF_ANGLE - angle)) / M_PI;
  } else {
    redValue = 0;
    greenValue = 0;
    blueValue = 0;
  }
}


// ********************************************
// ******************* MAIN *******************
// ********************************************

void setup() {
  // Initialize the 3 digital output pins for red, green, and blue
  pinMode(RED_PIN, OUTPUT);
  pinMode(GREEN_PIN, OUTPUT);
  pinMode(BLUE_PIN, OUTPUT);

  // Initialize serial
  Serial.begin(9600);
}

void loop() {
  // Read joystick X and Y analog values
  int joystickXValue = analogRead(JOYSTICK_VX_PIN);
  int joystickYValue = analogRead(JOYSTICK_VY_PIN);

  Serial.print("Joystick position: (" + String(joystickXValue) + ", " + String(joystickYValue) + ")  ");

  // Convert X,Y value into RGB
  colorWheelTransform(joystickXValue, joystickYValue);

  // Print the RGB being set
  Serial.println("Color: (R:" + String(redValue) + ", G:" + String(greenValue) + ", B:" + String(blueValue) + ")");

  // Set the color
  analogWrite(RED_PIN, redValue);
  analogWrite(GREEN_PIN, greenValue);
  analogWrite(BLUE_PIN, blueValue);

  // Slow down
  delay(500);
}
