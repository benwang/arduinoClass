// Pin definitions here
// These MUST be PWM pins. For Arduino UNO, choose from: 3, 5, 6, 9, 10, 11
const int RED_PIN = 6;
const int GREEN_PIN = 5;
const int BLUE_PIN = 3;

// This function prints the valid color options
void printHelpText() {
  Serial.println("Valid colors are: red, yellow, green, aqua, blue, violet, white, off");
  Serial.println("You can also specify any color with RGB values with: rgb 255 128 0");
  Serial.println("Please type a color and hit enter.");
}

void setup() {
  // Initialize the 3 digital output pins for red, green, and blue
  pinMode(RED_PIN, OUTPUT);
  pinMode(GREEN_PIN, OUTPUT);
  pinMode(BLUE_PIN, OUTPUT);

  // Initialize serial
  Serial.begin(9600);

  // Initial prompt
  printHelpText();
}

void loop() {
  // Good practice to declare and initialize variables at beginning of function
  String inputString;
  int redValue = 0;
  int greenValue = 0;
  int blueValue = 0;

  // Only process if there is data available on serial
  if (Serial.available()) {   
    // Read the characters available via serial as a string object
    inputString = Serial.readString();

    // Get rid of any extraneous newline characters (like if serial monitor is set to Newline mode)
    inputString.replace("\n", "");

    // Convert all characters to lowercase ones
    inputString.toLowerCase();

    // Check for preprogrammed color using if-statements
    if (inputString == "red") {
        redValue = 255;
        greenValue = 0;
        blueValue = 0;
    } else if (inputString == "yellow") {
        redValue = 255;
        greenValue = 255;
        blueValue = 0;
    } else if (inputString == "green") {
        redValue = 0;
        greenValue = 255;
        blueValue = 0;
    } else if (inputString == "aqua") {
        redValue = 0;
        greenValue = 255;
        blueValue = 255;
    } else if (inputString == "blue") {
        redValue = 0;
        greenValue = 0;
        blueValue = 255;
    } else if (inputString == "violet") {
        redValue = 255;
        greenValue = 0;
        blueValue = 255;
    } else if (inputString == "white") {
        redValue = 255;
        greenValue = 255;
        blueValue = 255;
    } else if (inputString == "off") {
        redValue = 0;
        greenValue = 0;
        blueValue = 0;
    } else if (inputString.startsWith("rgb")) {
        // The 4th character should be a space, we'll start looking at the 5th character (index of 4 because C++ indices start at 0)
        int numStartIndex = 4;
        int numEndIndex = inputString.indexOf(" ", numStartIndex);
        String valueString = inputString.substring(numStartIndex, numEndIndex);
        redValue = valueString.toInt();

        numStartIndex = numEndIndex + 1;
        numEndIndex = inputString.indexOf(" ", numStartIndex);
        valueString = inputString.substring(numStartIndex, numEndIndex);
        greenValue = valueString.toInt();

        numStartIndex = numEndIndex + 1;
        numEndIndex = inputString.indexOf(" ", numStartIndex);
        valueString = inputString.substring(numStartIndex, numEndIndex);
        blueValue = valueString.toInt();

    } else {
        Serial.println("ERROR: Unrecognized color \'" + inputString + "\'");
        printHelpText();
    }

    // Check that RGB values are in-range
    if (redValue > 255 || redValue < 0 || blueValue > 255 || blueValue < 0 || greenValue > 255 || greenValue  < 0) {
      Serial.println("ERROR: RGB values must be in the range 0 through 255, inclusive!");
      printHelpText();
    } else {
      // Print the RGB being set
      Serial.println("Setting color to R:" + String(redValue) + " G:" + String(greenValue) + " B:" + String(blueValue));
  
      // Set the color
      analogWrite(RED_PIN, redValue);
      analogWrite(GREEN_PIN, greenValue);
      analogWrite(BLUE_PIN, blueValue);
    }
  }
}
