// Set this to the pin your LED is plugged into
const int LED_PIN = 3;
int brightness = 1;
int brightnessStep = 1;

void setup() {
  // Initialize LED pin
  pinMode(LED_PIN, OUTPUT);
}

void loop() {
  // Write the brightness
  analogWrite(LED_PIN, brightness);

  // Check if we're at max or min brightness
  // "||" is the syntax for "or" in conditional statements
  if (brightness == 0 || brightness == 255) {
    // Flip the sign on the step (-1 becomes 1, 1 becomes -1)
    // "*=" is shorthand for "brightnessSte = brightnessStep * -1;"
    brightnessStep *= -1;
  }

  // Increment or decrement the brightness accordingly
  // "+=" is shorthand for "brightness = brightness + brightnessStep;"
  brightness += brightnessStep;

  // Refresh period in milliseconds (ms)
  delay(10);
}
