// Pin definitions here
// These MUST be PWM pins. For Arduino UNO, choose from: 3, 5, 6, 9, 10, 11
const int RED_PIN = 6;
const int GREEN_PIN = 5;
const int BLUE_PIN = 3;

// This function prints the valid color options
void printHelpText() {
  Serial.println("Valid colors are: red, yellow, green, aqua, blue, violet, white, off");
  Serial.println("Please type a color and hit enter.");
}

void setup() {
  // Initialize the 3 digital output pins for red, green, and blue
  pinMode(RED_PIN, OUTPUT);
  pinMode(GREEN_PIN, OUTPUT);
  pinMode(BLUE_PIN, OUTPUT);

  // Initialize serial
  Serial.begin(9600);

  // Initial prompt
  printHelpText();
}

void loop() {
  // Good practice to declare and initialize variables at beginning of function
  String inputString;
  int redValue = 0;
  int greenValue = 0;
  int blueValue = 0;

  // Only process if there is data available on serial
  if (Serial.available()) {   
    // Read the characters available via serial as a string object
    inputString = Serial.readString();

    // Get rid of any extraneous newline characters (like if serial monitor is set to Newline mode)
    inputString.replace("\n", "");

    // Check for preprogrammed color using if-statements
    if (inputString == "red") {
        redValue = 255;
        greenValue = 0;
        blueValue = 0;
    } else if (inputString == "yellow") {
        redValue = 255;
        greenValue = 255;
        blueValue = 0;
    } else if (inputString == "green") {
        redValue = 0;
        greenValue = 255;
        blueValue = 0;
    } else if (inputString == "aqua") {
        redValue = 0;
        greenValue = 255;
        blueValue = 255;
    } else if (inputString == "blue") {
        redValue = 0;
        greenValue = 0;
        blueValue = 255;
    } else if (inputString == "violet") {
        redValue = 255;
        greenValue = 0;
        blueValue = 255;
    } else if (inputString == "white") {
        redValue = 255;
        greenValue = 255;
        blueValue = 255;
    } else if (inputString == "off") {
        redValue = 0;
        greenValue = 0;
        blueValue = 0;
    } else {
        Serial.println("ERROR: Unrecognized color \'" + inputString + "\'");
        printHelpText();
    }

    // Set the color
    analogWrite(RED_PIN, redValue);
    analogWrite(GREEN_PIN, greenValue);
    analogWrite(BLUE_PIN, blueValue);
  }
}
