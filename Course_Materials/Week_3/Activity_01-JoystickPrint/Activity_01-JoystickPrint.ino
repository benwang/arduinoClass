// Joystick pins
const int JOYSTICK_VX_PIN = A1;
const int JOYSTICK_VY_PIN = A0;

void setup() {
  // Initialize serial
  Serial.begin(115200);
}

void loop() {
  // Read joystick X and Y analog values
  int joystickXValue = analogRead(JOYSTICK_VX_PIN);
  int joystickYValue = analogRead(JOYSTICK_VY_PIN);

  Serial.println("x:" + String(joystickXValue) + " y:" + String(joystickYValue));

  // Slow down
  delay(10);
}
