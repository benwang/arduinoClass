// Week 3 Homework Solution
// Motion Detector

// Using the ultrasonic sensor and buzzer, create a motion detector.
// When something is detected within 50 cm of the sensor more than 3
// cycles in a row, play a sound on the buzzer for 2 seconds.

// Pin definitions
const int ECHO_PIN = 2; // attach pin D2 Arduino to pin Echo of HC-SR04
const int TRIG_PIN = 3; //attach pin D3 Arduino to pin Trig of HC-SR04
const int BUZZER_PIN = 8;  // attach pin D8 to the positive (+) pin of the buzzer

// Configurable threshold at which the alarm goes off
const int TRIGGER_DISTANCE = 50; // cm

// Number of cycles less than TRIGGER_DISTANCE required to trigger alarm
const int NUM_CYCLES_TRIGGER_THRESHOLD = 3;

// Variable for tracking number of cycles that distance has been less
int numCyclesInRange;

// Function to sample the ultrasonic sensor
float sampleUltrasonicDistance() {
  // Clears the TRIG_PIN condition
  digitalWrite(TRIG_PIN, LOW);
  delayMicroseconds(2);

  // Sets the TRIG_PIN HIGH (ACTIVE) for 10 microseconds
  digitalWrite(TRIG_PIN, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN, LOW);

  // Reads the ECHO_PIN, returns the sound wave travel time in microseconds
  unsigned long duration = pulseIn(ECHO_PIN, HIGH);

  // Calculating the distance
  float distance = duration * 0.034 / 2; // Speed of sound wave divided by 2 (go and back)

  return distance;
}

void setup() {
  pinMode(TRIG_PIN, OUTPUT);
  pinMode(ECHO_PIN, INPUT);
  pinMode(BUZZER_PIN, OUTPUT);

  Serial.begin(115200);

  // Initialize variables
  numCyclesInRange = 0;
}

void loop() {
  float distance = sampleUltrasonicDistance();
  Serial.print("ultrasonicDistCM:" + String(distance));

  // Check if current sensor reading is within range
  if (distance < TRIGGER_DISTANCE) {
    // Increment the number of cycles in range by 1
    numCyclesInRange = numCyclesInRange + 1;
  } else {
    numCyclesInRange = 0;
  }
  
  Serial.println(" numCyclesInRange:" + String(numCyclesInRange));

  // Play the tone if we have reached 3 cycles
  if (numCyclesInRange >= NUM_CYCLES_TRIGGER_THRESHOLD) {
    // Play frequency 440 on the digital pin BUZZER_PIN for 2000ms (2 seconds)
    tone(BUZZER_PIN, 440, 2000);
    
    // Wait for the note to finish
    delay(2000);
    
    // stop the tone playing:
    noTone(BUZZER_PIN);
  }

  // Add some delay
  delay(50);
}
