// Ultrasonic Theramin

#include "pitches.h"

// Pin definitions
const int ECHO_PIN = 2; // attach pin D2 Arduino to pin Echo of HC-SR04
const int TRIG_PIN = 3; //attach pin D3 Arduino to pin Trig of HC-SR04
const int BUZZER_PIN = 8;  // attach pin D8 to the positive (+) pin of the buzzer

// Ultrasonic configuration
const float MIN_DIST_CM = 10.0; // cm, maps to lowest note
const float MAX_DIST_CM = 65.0; // cm, maps to highest note

// Note configuration
int notes[] =        {NOTE_C4, NOTE_D4, NOTE_E4, NOTE_F4, NOTE_G4, NOTE_A4, NOTE_B4, NOTE_C5};
String noteNames[] = {"NOTE_C4", "NOTE_D4", "NOTE_E4", "NOTE_F4", "NOTE_G4", "NOTE_A4", "NOTE_B4", "NOTE_C5"};
const int CYCLE_PERIOD_MS = 50; // Duration of each note

// Array for moving average
const unsigned int NUM_ELTS = 10;           // Adjustable value for how many samples to keep
unsigned int movingAvgIdx = 0;              // Array index in circular buffer
float movingAverageWindow[NUM_ELTS] = {0};  // Circular buffer for storing the last 10 samples

// Function to calculate average of array
float arrayAverage() {
  float sum = 0;

  // Calculate the sum of the array
  for (int i = 0; i < NUM_ELTS; i++) {
    sum += movingAverageWindow[i];
  }

  // Return average
  return sum / NUM_ELTS;
}

// Function to sample the ultrasonic sensor
float sampleUltrasonicDistance() {
  // Clears the TRIG_PIN condition
  digitalWrite(TRIG_PIN, LOW);
  delayMicroseconds(2);
  
  // Sets the TRIG_PIN HIGH (ACTIVE) for 10 microseconds
  digitalWrite(TRIG_PIN, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN, LOW);
  
  // Reads the ECHO_PIN, returns the sound wave travel time in microseconds
  unsigned long duration = pulseIn(ECHO_PIN, HIGH);
  
  // Calculating the distance
  float distance = duration * 0.034 / 2; // Speed of sound wave divided by 2 (go and back)

  // Add the current distance to the array
  movingAverageWindow[movingAvgIdx] = distance;

  // Increment the moving average index. This is a circular buffer of size 10, so we roll over back to 0
  movingAvgIdx = (movingAvgIdx + 1) % NUM_ELTS;
  
  // Return the moving average
  return arrayAverage();
}

void setup() {
  pinMode(TRIG_PIN, OUTPUT);
  pinMode(ECHO_PIN, INPUT);
  pinMode(BUZZER_PIN, OUTPUT);

  Serial.begin(115200);
}

void loop() {
  float distance = sampleUltrasonicDistance();
  Serial.print("ultrasonicDistCM:" + String(distance));

  // Play tone
  if (distance >= MIN_DIST_CM && distance <= MAX_DIST_CM) {
    // Map the distance to a musical note
    int noteIndex = map(distance, MIN_DIST_CM, MAX_DIST_CM, 0, sizeof(notes)/sizeof(int));
    int noteFreq = notes[noteIndex];

    // Debug print
    Serial.println(" note:" + noteNames[noteIndex]);

    // Play the tone
    tone(BUZZER_PIN, noteFreq, CYCLE_PERIOD_MS);
  } else {
    // Silent if distance is not in range
    Serial.println(" noteIndex:N/A");
    noTone(BUZZER_PIN);
  }

  // Wait for duration of note
  delay(CYCLE_PERIOD_MS);
}
