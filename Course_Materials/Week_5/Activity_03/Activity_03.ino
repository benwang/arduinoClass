void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);

  // Initial prompt
  Serial.println("Please type an integer and hit enter: ");
}

void loop() {
  // put your main code here, to run repeatedly:
  if (Serial.available()) {   
    // Read the characters available via serial as a string object
    int inputInt = Serial.parseInt();

    // Determine if number is even or odd
    if (inputInt % 2 == 0) {
      Serial.println(String(inputInt) + " is even");
    } else {
      Serial.println(String(inputInt) + " is odd");
    }
  }
}
