# Week 1

July 23, 2020

## Before Class

1. **Download the Arduino IDE from their [website](https://www.arduino.cc/en/Main/Software).** If you're on Windows, download the Windows Installer, not the Windows app.
2. **Install the Zoom app on your phone** from the app store or from <a href="https://zoom.us/download#mobile_app" target="_blank">here</a>. Having your phone as a 2nd camera will be useful for debugging hardware throughout the class.
3. **Test the Arduino**. Before class, open your kit and take out the Arduino. Use the included USB cable to plug it into a USB phone charger (not your computer!). Make sure that the LED on the Arduino lights up and blinks. Immediately unplug the USB if the Arduino smokes, gets hot, or the USB charger makes sounds that are not normal. Follow the warnings below:

    **IMPORTANT:**

    - Your Arduino comes in a bag that protects against static electricity. Electronics are **very** sensitive to static and can be easily damaged.
        - Before you touch the circuit card or any of the exposed metal, touch the large metal USB jack on the circuit card to dissipate any static.
        - After you are done, store the Arduino back in its static bag. Close the bag or fold it over with the Arduino inside.

    - The solder used on the Arduino contains **lead**. Wash your hands thouroughly after handling the Arduino or other components, especially before eating or drinking.

## Meeting Info

We will meet on **Thursdays at 7:30pm CT/8:30pm ET** and last for approximately 1.5 to 2 hours. <b>Link to the class <a href="https://wustl-hipaa.zoom.us/j/93348244242" target="_blank">Zoom meeting</a></b>.

## During Class

**Objectives:**

- Technical setup - Everyone log in on your phones too. Make sure you disable audio on your phone!
- Intro to HW/SW
- Plug in for first time
- Open up the blink example in IDE, flash it on
- Intro to C++. Explain what blink.ino does
- Make the blink faster/slower. Now make a pattern
- Serial communications

**Course Material:**

- Week 1 slides available <a href="https://gitlab.com/benwang/arduinoClass/-/raw/master/Course_Materials/Week_1/Week_1_Slides.pdf?inline=true" target="_blank">here</a>
- Full Week 1 course materials <a href="https://gitlab.com/benwang/arduinoClass/-/tree/master/Course_Materials/Week_1" target="_blank">here</a>

## In-class Examples

### [Activity 1] Blink Example

1. Go to **File** > **Examples** > **01.Basics** > **Blink**

    ![Open Example](img/Week_1/01-BlinkExampleBrowse.png)

    <br>

2. Plug your Arduino into a USB port on your computer.

3. Go to Tools

    1. Make sure Board is set on Arduino Uno

    2. Select the Port that says Arduino Uno next to it (COM3 in my case)

        ![Port Settings](img/Week_1/02-SelectCOMPort.png)

4. Click the Upload button to upload your sketch (program) to the Arduino. It will start running as soon as the flashing (uplaoding) process completes.

    ![Upload Button](img/Week_1/03-Upload.png)

    Check out the LED marked "L" near the USB jack!

    ![LED On](img/Week_1/04-LED_On.png)

    ![LED Off](img/Week_1/05-LED_Off.png)


### [Activity 2] Blink, Modified

1. Modify Blink so that the LED flashes twice as fast.
2. Now make it stay on for 2 seconds, off for 1 second
3. Challenge:
    1. Now make it blink so fast you can’t see the flashing (5ms on, 5ms off) 
    2. What if you want to make the LED dim? (1ms on, 9ms off)


<details>
    <summary><b>Solution 1</b></summary>
    
```c++
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(500);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(500);                       // wait for a second
}
```
</details>
<details>
    <summary><b>Solution 2</b></summary>
```c++
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(2000);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
}
```
</details>
<details>
    <summary><b>Solution 3a</b></summary>

```c++
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(5);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(5);                       // wait for a second
}
```
</details>
<details>
    <summary><b>Solution 3b</b></summary>
```c++
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(9);                       // wait for a second
}
```
</details>

<br>

### [Activity 3] Serial 01 - Serial Hello World

Now we will create a simple hello world using serial.

1. Go to **File** > **New**
2. Type in or copy/paste the following code:

        void setup() {
            // put your setup code here, to run once:
            Serial.begin(9600);
        }

        void loop() {
            // put your main code here, to run repeatedly:
            Serial.println("Hello World");

            // Wait 1 second
            delay(1000);
        }

3. Save as `Activity_03-Serial_Hello_World.ino`
4. Upload the sketch
5. Open up serial monitor and check it out!

    ![Serial Monitor](img/Week_1/06-SerialMonitor.png)


### [Activity 4] Serial 02 - Parrot

Now let's modify Serial 01 to echo back what you say:

1. So we don't accidentally save over our work, let's now do a **Save As** `Activity_04-Parrot.ino`
2. Let's add a prompt to the user. In `setup()` add the following line after `Serial.begin(9600);`

        Serial.println("Please type some text and hit enter: ");

3. Now, let's add a line to read a string from serial. Change `loop()` to the following. `Serial.available()` (documentation <a href="https://www.arduino.cc/reference/en/language/functions/communication/serial/available/" target="_blank">here</a>) is used to check whether there are bytes of data available to be read on the serial input. We then use `Serial.readString()` to read those bytes as a string object.

        void loop() {
            // put your main code here, to run repeatedly:
            if (Serial.available()) {   
                // Read the characters available via serial as a string object
                String inputString = Serial.readString();

                // Echo back what you just said
                Serial.println("You said: " + inputString); 
            }
        }

4. Upload the sketch
5. Open up serial monitor, type something in the textbox at the top, and hit enter!

<details>
    <summary><b>Click for my full sketch</b></summary>

```c++
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);

  // Initial prompt
  Serial.println("Please type some text and hit enter: ");
}

void loop() {
  // put your main code here, to run repeatedly:
  if (Serial.available()) {   
    // Read the characters available via serial as a string object
    String inputString = Serial.readString();

    // Echo back what you just said
    Serial.println("You said: " + inputString); 
  }
}
```
</details>

<br>

### [Activity 5] Serial 03 - Double It

Lastly, for you to be able to complete the homework, we need a way to turn a string received via serial to be converted to an integer. Let's write a sketch that prints any number you type in serial, multiplied by 2.

Arduino has documentation for all of the built-in functions available here: <a href="https://www.arduino.cc/reference/en/" target="_blank">Language Reference</a>. Go there and click **Serial**. On the Serial page, click **parseInt()** to see what it's used for:

> **Description**
> 
> Looks for the next valid integer in the incoming serial. The function terminates if it times out (see Serial.setTimeout()).
>
> **Returns**
> 
> The next valid integer. Data type: long.

So we can use this instead of `Serial.readString()` to read an integer from serial.

1. So we don't accidentally save over our work, let's now do a **Save As** `Activity_05-Double_it.ino`
2. Modify the serial print statement in `setup()` so that instead of "Please type some text and hit enter: ", it now says "Please type an integer and hit enter: "
3. In `loop()`:

    1. Change the `Serial.readString()` to `Serial.parseInt()` and the variable type from `String` to `int`.
    2. Change the serial print statement from `Serial.println("You said: " + inputString);` to `Serial.println(String(inputInt) + " times two is " + String(inputInt * 2));`

        Note that here, we wrap our integer with `String()`, which is a function that converts an object into a String object. If you leave this out, you'll see that either nothing shows up or you end up with a weird character! More on this later...

4. Upload the sketch
5. Open up serial monitor, type a number (an integer) in the textbox at the top, and hit enter!

<details>
    <summary><b>Click for my full sketch</b></summary>
```c++
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);

  // Initial prompt
  Serial.println("Please type an integer and hit enter: ");
}

void loop() {
  // put your main code here, to run repeatedly:
  if (Serial.available()) {   
    // Read the characters available via serial as a string object
    int inputInt = Serial.parseInt();

    // Echo back what you just said
    Serial.println(String(inputInt) + " times two is " + String(inputInt * 2));
  }
}
```
</details>
<br>

However, we have an issue where there seems to always be an extra 0 that is read. For example:

    999 times two is 1998
    0 times two is 0
    29 times two is 58
    0 times two is 0
    10 times two is 20
    0 times two is 0

We can solve this in several ways. For now, however, we'll do it the easy way. In the bottom-right corner of your serial monitor, change the first dropdown menu from "Newline" to "No line ending".

![No Line Ending](img/Week_1/07-SerialMonitorNoLineEnding.png)

## Homework

Create an Arduino sketch that uses the USB serial interface and the built-in LED. Every time you type a number and hit enter, the Arduino will have the LED stay on for that many seconds.