# Week 4

<!-- JS for copy code to clipboard button -->
<script src="../js/clipboard.min.js"></script>

August 13, 2020

## Meeting Info

We will meet on **Thursdays at 7:30pm CT/8:30pm ET** and last for approximately 1.5 to 2 hours. <b>Link to the class <a href="https://wustl-hipaa.zoom.us/j/93348244242" target="_blank">Zoom meeting</a></b>.

## Objectives

**Software:**

- Recap Week 1-3 concepts. Data types, functions, variables, logic (if-then-else)
- Programming basics!
    - if statements
    - while loops
    - for loops

## During Class

**Week 3 Homework**

Example solution to Week 3 homework available <a href="https://gitlab.com/benwang/arduinoClass/-/blob/master/Course_Materials/Week_3/Week_3_Homework_Solution/Week_3_Homework_Solution.ino" target="_blank">here</a>.

**Additional Resources**

Additional resources (tutorials, example code, documentation) is available for the kit at Elegoo's <a href="https://www.elegoo.com/tutorial/Elegoo%20Super%20Starter%20Kit%20for%20UNO%20V2.0.2020.5.13.zip" target="_blank">website</a>.

### Loops

Loops are used for repeating things.

#### While Loop

Repeat until the condition is met. For example:

```c++
int x = 0;
while (x < 10) {
    // Do something here

    x++;
}
```

Note that `x++` is simply shorthand for `x = x + 1`. Another way to write the same statement is `x += 1`.

Another common use of loops is in programs that run indefinitely (such as Arduino or video games!). For example, the loop() function in Arduino can also be implemented as:

```c++
void setup() {
    // Setup tasks here

    while (true) {
        // Loop tasks here
    }
}
```

#### For Loop

A modified while loop that combines the above lines into 1:

```c++
for (int x = 0; x < 10; x++) {
    // Do something here
    // It will repeat 10 times, for example:
    Serial.println(x);
}
```

The above would print:

```
0
1
2
3
4
5
6
7
8
9
```

### Conditional Statements



#### If statements

Used in a situation where you want to execute some code only when a condition is true. The "condition" is what's inside the parentheses. For example:

```c++
if (input == "hello") {
    Serial.println("goodbye");
}
```

#### Comparisons

Now is a good time to review the comparison operators in C++. These are very common across programming languages:

| Operator | Meaning | Explanation |
|-|-|-|
| == | Equality | True if left and right are equal. False otherwise |
| != | Inequality | True only if the left and right are not equal |
| > | Greater than | True if left is greater than right |
| < | Less than | True if left is less than right |
| >= | Greater than or equal to | True if left is greater than or equal to the right |
| <= | Less than or equal to | True if left is less than or equal to the right |

#### Structure

You may have noticed that C++ uses braces `{}` heavily. C++ does not care about whitespace (spaces, new lines, tabs). Your entire program could be written on one line. Braces are used to group code. For example, the entire "then" block of code is wrapped by a set of braces.

Loops and functions also uses braces too.

Note that if only 1 line follows a if-statement or loop, the braces are optional.

For example, the following 3 chunks of code are identical:

```c++
if (x != 5) {
    Serial.println("x is not five");
}
```

```c++
if (x != 5)
    Serial.println("x is not five");
```

```c++
if (x != 5) Serial.println("x is not five");
```

#### If-then-else

You might want to do something else if the condition is not true:

```c++
if (input == "hello") {
    Serial.println("goodbye");
} else {
    Serial.println("Say hi to me!");
}
```

#### Else-if

You can also nest conditions. We used this in Week 2 for defining the RGB color of the LED:

```c++
if (inputString == "red") {
    redValue = 255;
    greenValue = 0;
    blueValue = 0;
} else if (inputString == "yellow") {
    redValue = 255;
    greenValue = 255;
    blueValue = 0;
} else if (inputString == "green") {
    redValue = 0;
    greenValue = 255;
    blueValue = 0;
} else if (inputString == "aqua") {
    redValue = 0;
    greenValue = 255;
    blueValue = 255;
} else {
    // ...
}
```

## In-class Activities

### [ Activity 1 ] One-Shot Counter

Have the Arduino count 1 to 10, printing the count once per second, and then stop.

<details>
<summary><b>Click Here for a Hint</b></summary>
<br>
<p style="color: blue">Use a loop in `setup()` and have `loop()` do nothing.</p>
</details>
<br>

*Example solution available <a href="https://gitlab.com/benwang/arduinoClass/-/blob/master/Course_Materials/Week_4/Activity1/Activity1.ino" target="_blank">here</a>*

<br>

### [ Activity 2 ] Print a Pyramid

Similar to Activity 1, let's print something cool on start-up.

1. Have the Arduino print a basic 5x5 triangle:

        *
        **
        ***
        ****
        *****

    Note: There's an easy way and a harder way. If you did this by hard-coding in the pattern using strings, try it again using loops.

    Hint: Start by using 2 for-loops to print a 5x5 grid of *'s. Then, modify the condition of the inner for-loop so that it depends on the row number as an end condition.

    *Example solution available <a href="https://gitlab.com/benwang/arduinoClass/-/blob/master/Course_Materials/Week_4/Activity2-1/Activity2-1.ino" target="_blank">here</a>*

    <br>

2. Now invert the triangle from step 1 so that the top-right corner of the box is filled:

        *****
         ****
          ***
           **
            *

    *Example solution available <a href="https://gitlab.com/benwang/arduinoClass/-/blob/master/Course_Materials/Week_4/Activity2-2/Activity2-2.ino" target="_blank">here</a>*

    <br>

3. Can you have it go wide and then back down?

        *
        **
        ***
        ****
        *****
        ****
        ***
        **
        *

4. [Challenge] Can you draw a centered pyramid?

            *
           ***
          *****
         ********
        **********

Reference: [https://www.arduino.cc/reference/en/language/functions/random-numbers/random/](https://www.arduino.cc/reference/en/language/functions/random-numbers/random/)