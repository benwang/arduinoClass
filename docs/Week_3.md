# Week 3

<!-- JS for copy code to clipboard button -->
<script src="../js/clipboard.min.js"></script>

August 6, 2020

## Meeting Info

We will meet on **Thursdays at 7:30pm CT/8:30pm ET** and last for approximately 1.5 to 2 hours. <b>Link to the class <a href="https://wustl-hipaa.zoom.us/j/93348244242" target="_blank">Zoom meeting</a></b>.

## Objectives

**Hardware:**

- Ultrasonic sensor
- Buzzer

**Software:**

- Recap Week 1+2 concepts? Data types, functions, variables, logic (if-then-else)

## During Class

**Week 2 Homework**

Example solution to Week 2 homework available <a href="https://gitlab.com/benwang/arduinoClass/-/blob/master/Course_Materials/Week_2/Week_2_Homework_Solution/Week_2_Homework_Solution.ino" target="_blank">here</a>.

**Course Material:**

- Full Week 3 course materials <a href="https://gitlab.com/benwang/arduinoClass/-/tree/master/Course_Materials/Week_3" target="_blank">here</a>

**Questions from Week 2**

- **Pronunciation:** arr-do-**WE**-no
- Why are the additive light primary colors different from the primary colors in art?
    - Difference is because one is additive, one is subtractive.
    - In the additive color model, we are starting from black and adding light to create all colors (through white)
    - In the subtractive color model, we start with white (the light reflecting on the surface) and subtracting away wavelengths to achieve color (through black)
    - <a href="http://learn.leighcotnoir.com/artspeak/elements-color/primary-colors" target="_blank">More information here</a>
- Note that when you upload a program, it stays on across power cycles!

## In-class Examples

This week, we'll be using:

- Piezo buzzer
- Ultrasonic sensor
- Python

### [ Activity 1 ] Joystick + Python

1. Download the Week 3 GUI (Graphical User Interface) application:

    - On **Windows**, download the .zip file from <a href="https://gitlab.com/benwang/arduinoClass/-/raw/master/Course_Materials/Week_3/Week3GUI/Week3GUI-Windows.zip?inline=false" target=_blank">here</a>. Once it downloads, browse to it in File Explorer, right-click and click **Extract All**. You can select the defaults Downloads folder as the destination.
    - On **Mac**, run the following in a terminal (the repository will be downloaded to your Desktop):

            cd ~/Desktop
            git clone git@gitlab.com:benwang/ArduinoClassWeek3GUI.git
            cd ArduinoClassWeek3GUI
            ./install_env.sh
            source venv/bin/activate

2. Open the Arduino IDE, go to **File** > **New**. Replace the code with the following (very similar to Week 2 Activity 6). If you already took apart your circuit, follow the steps from [Week 2 Activity 6](Week_2.md#actvitiy-6-analog-input-joystick) to reassemble it.

    <button class="btn" id="copy-button" data-clipboard-target="#targetActivity1">Click to Copy Code Below</button>
    <pre xml:space="preserve" id="targetActivity1">

        // Joystick pins
        const int JOYSTICK_VX_PIN = A1;
        const int JOYSTICK_VY_PIN = A0;

        void setup() {
            // Initialize serial
            Serial.begin(115200);
        }

        void loop() {
            // Read joystick X and Y analog values
            int joystickXValue = analogRead(JOYSTICK_VX_PIN);
            int joystickYValue = analogRead(JOYSTICK_VY_PIN);

            Serial.println("joystickX:" + String(joystickXValue) + " joystickY:" + String(joystickYValue));

            // Slow down
            delay(10);
        }
    </pre>
    <!-- Need this line here to workd -->
    <script>new ClipboardJS('.btn');</script>

    Note that the "baud" rate of the serial has increased from 9600 to 115200. This means that instead of sending characters (or bytes) at 9,600 bytes per second, we'll now communicate faster, at 115,200 bytes per second.

3. Now run the Python application and move the joystick!

    - On Windows, in File Explorer, open the `Week3GUI-Windows` folder that was extracted in Step 1. Double click the `Week3GUI.exe` application.
    - On Mac, run the following command in terminal (**after** running the `source venv/bin/activate` line in Step 1)

            python Week3GUI.py
    
    - It should automatically detect the Arduino's port and connect. If not, make sure the Arduino is plugged in on USB and click **Detect ports**.
    - If it says that it failed after 2 tries but data did show up (eg., "joystickX:512 joystickY:512"), then select the same port (or baud rate) again to make it try again.

This concludes our work with the joystick and multicolor LED at this time. You may disassemble everything on the breadboard. Make sure to keep your 330 Ohm resistors somewhere where you'll remember their resistance value (otherwise you'll have to look at the colored stripes on them to figure out what their resistance was again).

### [ Activity 2 ] Play a song

1. You will need:
    - Qty 1 - short black wire
    - Qty 1 - short green wire
    - Qty 1 - 330 Ohm resistor
    - Qty 1 - Passive buzzer (no sticker on front, green PCB or printed circuit board visible from back)
2. Using the components, build the following circuit:

    ![Buzzer Wiring](img/Week_3/Buzzer_Wiring_bb.png)

3. Create a new sketch in the Arduino IDE (File > New) and replace it with the following code. Note that there is a 2nd file you need!

    <button class="btn" id="copy-button" data-clipboard-target="#targetActivity2a">Click to Copy Code Below</button>
    <pre xml:space="preserve" id="targetActivity2a">

        /*
        Melody

        Plays a melody

        circuit:
        - 8 ohm speaker on digital pin 8

        created 21 Jan 2010
        modified 30 Aug 2011
        by Tom Igoe

        This example code is in the public domain.

        http://www.arduino.cc/en/Tutorial/Tone
        */

        #include "pitches.h"

        // notes in the melody:
        int melody[] = {
            NOTE_C4, NOTE_G3, NOTE_G3, NOTE_A3, NOTE_G3, 0, NOTE_B3, NOTE_C4
        };

        // note durations: 4 = quarter note, 8 = eighth note, etc.:
        int noteDurations[] = {
            4, 8, 8, 4, 4, 4, 4, 4
        };

        void setup() {
            // iterate over the notes of the melody:
            for (int thisNote = 0; thisNote < 8; thisNote++) {

                // to calculate the note duration, take one second divided by the note type.
                //e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.
                int noteDuration = 1000 / noteDurations[thisNote];
                tone(8, melody[thisNote], noteDuration);

                // to distinguish the notes, set a minimum time between them.
                // the note's duration + 30% seems to work well:
                int pauseBetweenNotes = noteDuration * 1.30;
                delay(pauseBetweenNotes);
                // stop the tone playing:
                noTone(8);
            }
        }

        void loop() {
        // no need to repeat the melody.
        }
    </pre>


    Now in the IDE, create a new file. Click the drop-down arrow underneath the **Serial Monitor** and click **New Tab**. Name the file "pitches.h". Paste in the following:

    <button class="btn" id="copy-button" data-clipboard-target="#targetActivity2b">Click to Copy Code Below</button>
    <pre xml:space="preserve" id="targetActivity2b">

        /*************************************************
        * Public Constants
        *************************************************/

        #define NOTE_B0  31
        #define NOTE_C1  33
        #define NOTE_CS1 35
        #define NOTE_D1  37
        #define NOTE_DS1 39
        #define NOTE_E1  41
        #define NOTE_F1  44
        #define NOTE_FS1 46
        #define NOTE_G1  49
        #define NOTE_GS1 52
        #define NOTE_A1  55
        #define NOTE_AS1 58
        #define NOTE_B1  62
        #define NOTE_C2  65
        #define NOTE_CS2 69
        #define NOTE_D2  73
        #define NOTE_DS2 78
        #define NOTE_E2  82
        #define NOTE_F2  87
        #define NOTE_FS2 93
        #define NOTE_G2  98
        #define NOTE_GS2 104
        #define NOTE_A2  110
        #define NOTE_AS2 117
        #define NOTE_B2  123
        #define NOTE_C3  131
        #define NOTE_CS3 139
        #define NOTE_D3  147
        #define NOTE_DS3 156
        #define NOTE_E3  165
        #define NOTE_F3  175
        #define NOTE_FS3 185
        #define NOTE_G3  196
        #define NOTE_GS3 208
        #define NOTE_A3  220
        #define NOTE_AS3 233
        #define NOTE_B3  247
        #define NOTE_C4  262
        #define NOTE_CS4 277
        #define NOTE_D4  294
        #define NOTE_DS4 311
        #define NOTE_E4  330
        #define NOTE_F4  349
        #define NOTE_FS4 370
        #define NOTE_G4  392
        #define NOTE_GS4 415
        #define NOTE_A4  440
        #define NOTE_AS4 466
        #define NOTE_B4  494
        #define NOTE_C5  523
        #define NOTE_CS5 554
        #define NOTE_D5  587
        #define NOTE_DS5 622
        #define NOTE_E5  659
        #define NOTE_F5  698
        #define NOTE_FS5 740
        #define NOTE_G5  784
        #define NOTE_GS5 831
        #define NOTE_A5  880
        #define NOTE_AS5 932
        #define NOTE_B5  988
        #define NOTE_C6  1047
        #define NOTE_CS6 1109
        #define NOTE_D6  1175
        #define NOTE_DS6 1245
        #define NOTE_E6  1319
        #define NOTE_F6  1397
        #define NOTE_FS6 1480
        #define NOTE_G6  1568
        #define NOTE_GS6 1661
        #define NOTE_A6  1760
        #define NOTE_AS6 1865
        #define NOTE_B6  1976
        #define NOTE_C7  2093
        #define NOTE_CS7 2217
        #define NOTE_D7  2349
        #define NOTE_DS7 2489
        #define NOTE_E7  2637
        #define NOTE_F7  2794
        #define NOTE_FS7 2960
        #define NOTE_G7  3136
        #define NOTE_GS7 3322
        #define NOTE_A7  3520
        #define NOTE_AS7 3729
        #define NOTE_B7  3951
        #define NOTE_C8  4186
        #define NOTE_CS8 4435
        #define NOTE_D8  4699
        #define NOTE_DS8 4978
    </pre>

4. Upload the sketch. Enjoy!

Activity source: [https://www.arduino.cc/en/Tutorial/ToneMelody?from=Tutorial.Tone](https://www.arduino.cc/en/Tutorial/ToneMelody?from=Tutorial.Tone)

### [ Challenge ][ Activity 3 ] Serial tone generator

Now modify the sketch (and Save As `Activity_03-Serial_Tone_Generator`) so that it does the following:

1. Prompts the user to enter a frequency via serial
2. Reads the frequency the user types on serial as an integer (See [Week 1 Activity 6](Week_1.md#activity-5-serial-03-double-it))
3. Plays that tone for 1 second

### [ Activity 4 ] Ultrasonic Sensor Basics

Leaving the buzzer circuit in place (we'll need it later), add the circuitry for the ultrasonic distance sensor. This sensor has 2 cylindrical devices on it. One is a speaker and the other is a microphone. The speaker emits a sound and the time it takes for the echo to be heard is recorded. Multiplying the time for the echo by the speed of sound yields the distance the sound waves travelled. Divide that by 2 and you have the distance to the nearest object!

1. You will need:
    - Qty 1 - medium black wire
    - Qty 1 - medium red wire
    - Qty 1 - short white wire
    - Qty 1 - short orange wire
    - Qty 1 - HC-SR04 ultrasonic sensor
2. Using the components, build the circuit on the right side. Note that the ultrasonic sensor should plug directly into the breadboard. It's offset for clarity of the illustration.

    ![Theramin Wiring](img/Week_3/Theramin_Wiring_bb.png)

3. Create a new sketch in the Arduino IDE (**File** > **New**) and replace it with the following code.

    <button class="btn" id="copy-button" data-clipboard-target="#targetActivity4">Click to Copy Code Below</button>
    <pre xml:space="preserve" id="targetActivity4">

        // ---------------------------------------------------------------- //
        // Arduino Ultrasoninc Sensor HC-SR04
        // Re-writed by Arbi Abdul Jabbaar
        // Using Arduino IDE 1.8.7
        // Using HC-SR04 Module
        // Tested on 17 September 2019
        // ---------------------------------------------------------------- //

        #define echoPin 2 // attach pin D2 Arduino to pin Echo of HC-SR04
        #define trigPin 3 //attach pin D3 Arduino to pin Trig of HC-SR04

        // defines variables
        unsigned long duration; // variable for the duration of sound wave travel
        float distance; // variable for the distance measurement

        void setup() {
            pinMode(trigPin, OUTPUT); // Sets the trigPin as an OUTPUT
            pinMode(echoPin, INPUT); // Sets the echoPin as an INPUT
            
            Serial.begin(115200); // // Serial Communication is starting with 9600 of baudrate speed
            
            Serial.println("Ultrasonic Sensor HC-SR04 Test"); // print some text in Serial Monitor
            Serial.println("with Arduino UNO R3");
        }

        void loop() {
            // Clears the trigPin condition
            digitalWrite(trigPin, LOW);
            delayMicroseconds(2);
            
            // Sets the trigPin HIGH (ACTIVE) for 10 microseconds
            digitalWrite(trigPin, HIGH);
            delayMicroseconds(10);
            digitalWrite(trigPin, LOW);
            
            // Reads the echoPin, returns the sound wave travel time in microseconds
            duration = pulseIn(echoPin, HIGH);
            
            // Calculating the distance
            distance = duration * 0.034 / 2; // Speed of sound wave divided by 2 (go and back)
            
            // Displays the distance on the Serial Monitor
            Serial.println("ultrasonicDistCM:" + String(distance));

            // Slow down
            delay(50);
        }
    </pre>

4. Upload and open up a serial monitor. Wave you hand in front of it.

Source: [https://create.arduino.cc/projecthub/abdularbi17/ultrasonic-sensor-hc-sr04-with-arduino-tutorial-327ff6](https://create.arduino.cc/projecthub/abdularbi17/ultrasonic-sensor-hc-sr04-with-arduino-tutorial-327ff6)

### [ Activity 5 ] Ultrasonic Sensor GUI

Now, open up the GUI from [Activity 1](Week_3.md#activity-1-joystick-python) (double click Week3GUI on Windows or run `source venv/bin/activate` and `python Week3GUI.py` on Mac). If it says it fails after 2 tries, try changing the baud rate.

Questions:
- How does it look? Try moving your hand in front or pointing it at the ceiling.
- What's the distance to the ceiling? Is it accurate?
- Is the line smooth or rough? Is the noise (or roughness) an issue?

Yes, noise is a common issue. We can use filtering algorithms to smooth out the data and get more accurate readings.

Now, open the other application in the Week3GUI folder.

- On Windows, double click `Week3GUI_MovingAverage`
- On Mac, run `source venv/bin/activate` and `python Week3GUI_MovingAverage.py`

Which line (red or blue) do you prefer?

<!--Challenge: Let's implement a moving average filter (similar to the one used for the blue line) on the Arduino. Change -->

### [ Activity 6 ] Ultrasonic Theramin!

**Background:**

If you aren't familiar with Theramins: [https://www.youtube.com/watch?v=K6KbEnGnymk](https://www.youtube.com/watch?v=K6KbEnGnymk)

We'll make something similar, but using the ultrasonic sensor. Here's what we want:

- Tone frequency to be dependent upon the distance your hand is from the sensor
- When your hand is removed, the sound should stop (otherwise this will get rather annoying)

**Instructions:**

The circuit we'll need here was assembled in [Activity 4](Week_3.md#activity-4-ultrasonic-sensor-basics).

Because we'll need "pitches.h" again, start from the Activity 2 sketch. Open it and save as `Activity_06-Theramin`. Replace the main file (Activity_06-Theramin.ino). Upload and give it a try by moving your hand back and forth in front of the sensor.

<button class="btn" id="copy-button" data-clipboard-target="#targetActivity6">Click to Copy Code Below</button>
<pre xml:space="preserve" id="targetActivity6">

```c++
// Ultrasonic Theramin

#include "pitches.h"

// Pin definitions
const int ECHO_PIN = 2; // attach pin D2 Arduino to pin Echo of HC-SR04
const int TRIG_PIN = 3; //attach pin D3 Arduino to pin Trig of HC-SR04
const int BUZZER_PIN = 8;  // attach pin D8 to the positive (+) pin of the buzzer

// Ultrasonic configuration
const float MIN_DIST_CM = 10.0; // cm, maps to lowest note
const float MAX_DIST_CM = 65.0; // cm, maps to highest note

// Note configuration
int notes[] =        {NOTE_C4, NOTE_D4, NOTE_E4, NOTE_F4, NOTE_G4, NOTE_A4, NOTE_B4, NOTE_C5};
String noteNames[] = {"NOTE_C4", "NOTE_D4", "NOTE_E4", "NOTE_F4", "NOTE_G4", "NOTE_A4", "NOTE_B4", "NOTE_C5"};
const int CYCLE_PERIOD_MS = 50; // Duration of each note

// Function to sample the ultrasonic sensor
float sampleUltrasonicDistance() {
  // Clears the TRIG_PIN condition
  digitalWrite(TRIG_PIN, LOW);
  delayMicroseconds(2);
  
  // Sets the TRIG_PIN HIGH (ACTIVE) for 10 microseconds
  digitalWrite(TRIG_PIN, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN, LOW);
  
  // Reads the ECHO_PIN, returns the sound wave travel time in microseconds
  unsigned long duration = pulseIn(ECHO_PIN, HIGH);
  
  // Calculating the distance
  float distance = duration * 0.034 / 2; // Speed of sound wave divided by 2 (go and back)

  return distance;
}

void setup() {
  pinMode(TRIG_PIN, OUTPUT);
  pinMode(ECHO_PIN, INPUT);
  pinMode(BUZZER_PIN, OUTPUT);

  Serial.begin(115200);
}

void loop() {
  float distance = sampleUltrasonicDistance();
  Serial.print("ultrasonicDistCM:" + String(distance));

  // Play tone
  if (distance >= MIN_DIST_CM && distance <= MAX_DIST_CM) {
    // Map the distance to a musical note
    int noteIndex = map(distance, MIN_DIST_CM, MAX_DIST_CM, 0, sizeof(notes)/sizeof(int));
    int noteFreq = notes[noteIndex];

    // Debug print
    Serial.println(" note:" + noteNames[noteIndex]);

    // Play the tone
    tone(BUZZER_PIN, noteFreq, CYCLE_PERIOD_MS);
  } else {
    // Silent if distance is not in range
    Serial.println(" noteIndex:N/A");
    noTone(BUZZER_PIN);
  }

  // Wait for duration of note
  delay(CYCLE_PERIOD_MS);
}
```
</pre>

### [ Challenge ][ Activity 7 ] Theramin Filtered

Now we'll implement the filter on the Arduino. This will help the sound played to be more stable.

1. Before the `sampleUltrasonicDistance()` function, add the following. This sets up the variables we'll use to track the last 10 samples and also adds a function to calculate the average of those 10 values.

    The `movingAverageWindow` array is called a "circular buffer", because we only track the last 10 points and want to forget the 11th, we keep track of which element is the "11th" so that we can overwrite it. Then we increment the `movingAvgIdx` variable so that at the next cycle, we overwrite that value and so on.

    <button class="btn" id="copy-button" data-clipboard-target="#targetActivity7a">Click to Copy Code Below</button>
    <pre xml:space="preserve" id="targetActivity7a">

        // Array for moving average
        const unsigned int NUM_ELTS = 10;           // Adjustable value for how many samples to keep
        unsigned int movingAvgIdx = 0;              // Array index in circular buffer
        float movingAverageWindow[NUM_ELTS] = {0};  // Circular buffer for storing the last 10 samples

        // Function to calculate average of array
        float arrayAverage() {
            float sum = 0;

            // Calculate the sum of the array
            for (int i = 0; i < NUM_ELTS; i++) {
                sum += movingAverageWindow[i];
            }

            // Return average
            return sum / NUM_ELTS;
        }
    </pre>

2. Now replace the last line of the `sampleUltrasonicDistance()` function (`return distance;`) with the following. This implements the rest of the circular buffer logic (overwriting the 11th value with the latest and calculate the average of the last 10 points.)

    <button class="btn" id="copy-button" data-clipboard-target="#targetActivity7b">Click to Copy Code Below</button>
    <pre xml:space="preserve" id="targetActivity7b">

        // Add the current distance to the array
        movingAverageWindow[movingAvgIdx] = distance;

        // Increment the moving average index. This is a circular buffer of size 10, so we roll over back to 0
        movingAvgIdx = (movingAvgIdx + 1) % NUM_ELTS;
        
        // Return the moving average
        return arrayAverage();
    </pre>

3. Upload and run. How does it feel? Open up the Week3GUI_MovingAverage again. Do the 2 lines look more similar now?
4. If you're up for it, try changing the `NUM_ELTS` variable from 10 to a higher number. Now what happens? 

<details>
    <summary><b>Full code</b></summary>

<button class="btn" id="copy-button" data-clipboard-target="#targetActivity7c">Click to Copy Code Below</button>
<pre xml:space="preserve" id="targetActivity7c">

```c++
// Ultrasonic Theramin

#include "pitches.h"

// Pin definitions
const int ECHO_PIN = 2; // attach pin D2 Arduino to pin Echo of HC-SR04
const int TRIG_PIN = 3; //attach pin D3 Arduino to pin Trig of HC-SR04
const int BUZZER_PIN = 8;  // attach pin D8 to the positive (+) pin of the buzzer

// Ultrasonic configuration
const float MIN_DIST_CM = 10.0; // cm, maps to lowest note
const float MAX_DIST_CM = 65.0; // cm, maps to highest note

// Note configuration
int notes[] =        {NOTE_C4, NOTE_D4, NOTE_E4, NOTE_F4, NOTE_G4, NOTE_A4, NOTE_B4, NOTE_C5};
String noteNames[] = {"NOTE_C4", "NOTE_D4", "NOTE_E4", "NOTE_F4", "NOTE_G4", "NOTE_A4", "NOTE_B4", "NOTE_C5"};
const int CYCLE_PERIOD_MS = 50; // Duration of each note

// Array for moving average
const unsigned int NUM_ELTS = 10;           // Adjustable value for how many samples to keep
unsigned int movingAvgIdx = 0;              // Array index in circular buffer
float movingAverageWindow[NUM_ELTS] = {0};  // Circular buffer for storing the last 10 samples

// Function to calculate average of array
float arrayAverage() {
  float sum = 0;

  // Calculate the sum of the array
  for (int i = 0; i < NUM_ELTS; i++) {
    sum += movingAverageWindow[i];
  }

  // Return average
  return sum / NUM_ELTS;
}

// Function to sample the ultrasonic sensor
float sampleUltrasonicDistance() {
  // Clears the TRIG_PIN condition
  digitalWrite(TRIG_PIN, LOW);
  delayMicroseconds(2);
  
  // Sets the TRIG_PIN HIGH (ACTIVE) for 10 microseconds
  digitalWrite(TRIG_PIN, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN, LOW);
  
  // Reads the ECHO_PIN, returns the sound wave travel time in microseconds
  unsigned long duration = pulseIn(ECHO_PIN, HIGH);
  
  // Calculating the distance
  float distance = duration * 0.034 / 2; // Speed of sound wave divided by 2 (go and back)

  // Add the current distance to the array
  movingAverageWindow[movingAvgIdx] = distance;

  // Increment the moving average index. This is a circular buffer of size 10, so we roll over back to 0
  movingAvgIdx = (movingAvgIdx + 1) % NUM_ELTS;
  
  // Return the moving average
  return arrayAverage();
}

void setup() {
  pinMode(TRIG_PIN, OUTPUT);
  pinMode(ECHO_PIN, INPUT);
  pinMode(BUZZER_PIN, OUTPUT);

  Serial.begin(115200);
}

void loop() {
  float distance = sampleUltrasonicDistance();
  Serial.print("ultrasonicDistCM:" + String(distance));

  // Play tone
  if (distance >= MIN_DIST_CM && distance <= MAX_DIST_CM) {
    // Map the distance to a musical note
    int noteIndex = map(distance, MIN_DIST_CM, MAX_DIST_CM, 0, sizeof(notes)/sizeof(int));
    int noteFreq = notes[noteIndex];

    // Debug print
    Serial.println(" note:" + noteNames[noteIndex]);

    // Play the tone
    tone(BUZZER_PIN, noteFreq, CYCLE_PERIOD_MS);
  } else {
    // Silent if distance is not in range
    Serial.println(" noteIndex:N/A");
    noTone(BUZZER_PIN);
  }

  // Wait for duration of note
  delay(CYCLE_PERIOD_MS);
}
```
</pre>
</details>
<br>

## Homework

Using the ultrasonic sensor and buzzer, create a motion detector. When something is detected within 50 cm of the sensor more than 3 cycles in a row, play a sound on the buzzer for 2 seconds.

Hint:

- Use a global variable (one that's declared at the top of the program, outside of any functions) to track the number of samples for which the `distance` has been less than 50 cm.
- Reset this variable to 0 any time `distance` is greater than 50 cm.
- If this variable is greater than 3, play a tone with something like this:

        // Play frequency 440 on the digital pin BUZZER_PIN for 2000ms (2 seconds)
        tone(BUZZER_PIN, 440, 2000);

        // Wait for the note to finish
        delay(2000);

        // stop the tone playing:
        noTone(BUZZER_PIN);

This one is a bit difficult! It relies on topics we haven't talked about (if-statements). This will be covered in Week 4. If you need help, here's my solution (based on Activity 6):

<details>
    <summary><b>Homework solution</b></summary>

```c++
// Week 3 Homework Solution
// Motion Detector

// Using the ultrasonic sensor and buzzer, create a motion detector.
// When something is detected within 50 cm of the sensor more than 3
// cycles in a row, play a sound on the buzzer for 2 seconds.

// Pin definitions
const int ECHO_PIN = 2; // attach pin D2 Arduino to pin Echo of HC-SR04
const int TRIG_PIN = 3; //attach pin D3 Arduino to pin Trig of HC-SR04
const int BUZZER_PIN = 8;  // attach pin D8 to the positive (+) pin of the buzzer

// Configurable threshold at which the alarm goes off
const int TRIGGER_DISTANCE = 50; // cm

// Number of cycles less than TRIGGER_DISTANCE required to trigger alarm
const int NUM_CYCLES_TRIGGER_THRESHOLD = 3;

// Variable for tracking number of cycles that distance has been less
int numCyclesInRange;

// Function to sample the ultrasonic sensor
float sampleUltrasonicDistance() {
  // Clears the TRIG_PIN condition
  digitalWrite(TRIG_PIN, LOW);
  delayMicroseconds(2);

  // Sets the TRIG_PIN HIGH (ACTIVE) for 10 microseconds
  digitalWrite(TRIG_PIN, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN, LOW);

  // Reads the ECHO_PIN, returns the sound wave travel time in microseconds
  unsigned long duration = pulseIn(ECHO_PIN, HIGH);

  // Calculating the distance
  float distance = duration * 0.034 / 2; // Speed of sound wave divided by 2 (go and back)

  return distance;
}

void setup() {
  pinMode(TRIG_PIN, OUTPUT);
  pinMode(ECHO_PIN, INPUT);
  pinMode(BUZZER_PIN, OUTPUT);

  Serial.begin(115200);

  // Initialize variables
  numCyclesInRange = 0;
}

void loop() {
  float distance = sampleUltrasonicDistance();
  Serial.print("ultrasonicDistCM:" + String(distance));

  // Check if current sensor reading is within range
  if (distance < TRIGGER_DISTANCE) {
    // Increment the number of cycles in range by 1
    numCyclesInRange = numCyclesInRange + 1;
  } else {
    numCyclesInRange = 0;
  }
  
  Serial.println(" numCyclesInRange:" + String(numCyclesInRange));

  // Play the tone if we have reached 3 cycles
  if (numCyclesInRange >= NUM_CYCLES_TRIGGER_THRESHOLD) {
    // Play frequency 440 on the digital pin BUZZER_PIN for 2000ms (2 seconds)
    tone(BUZZER_PIN, 440, 2000);
    
    // Wait for the note to finish
    delay(2000);
    
    // stop the tone playing:
    noTone(BUZZER_PIN);
  }

  // Add some delay
  delay(50);
}
```
</details>
<br>