# Week 6

<!-- JS for copy code to clipboard button -->
<script src="../js/clipboard.min.js"></script>

September 3, 2020

## Meeting Info

We will meet on **Thursdays at 7:30pm CT/8:30pm ET** and last for approximately 1.5 to 2 hours. <b>Link to the class <a href="https://wustl-hipaa.zoom.us/j/93348244242" target="_blank">Zoom meeting</a></b>.

## Objectives

**Software:**

- IR library
- Servo library

**Hardware:**

- IR Receiver
- IR Remote
- Servo motor

## During Class

**Additional Resources**

Additional resources (tutorials, example code, documentation) is available for the kit at Elegoo's <a href="https://www.elegoo.com/tutorial/Elegoo%20Super%20Starter%20Kit%20for%20UNO%20V2.0.2020.5.13.zip" target="_blank">website</a>.

### Infrared & Remote Controls

Have you ever held a camera (eg., smartphone camera) at a TV remote and pressed the button? *Note that your camera CANNOT have an infrared filter built-in. Try the front-facing camera if the rear one doesn't work.* You should be able see a quick flash of light when you click any button.

The flash of light is a digital signal. The single flash is actually many 1s and 0s, represenging a number. The TV and remote have an established "code" for each command. This is how universal remotes are able to communicate with a huge range of TVs, although you have to "program" the remote by telling it which set of codes to use.

Up close, it might look something like this:

![Oscilloscope image](img/Week_6/light_sonycodepulses.jpg)

Source: Adafruit

Adafruit's website has more information: <a href="https://learn.adafruit.com/ir-sensor/ir-remote-signals" target="_blank">IR Remote Signals</a>

### Servo Motors

Servos motors or "servos" are motors with position feedback. This allows you to command an angle and the servo will do whatever it takes (within its power) to reach and hold that position. Typical servos have a 180° range of motion.

![Servo Motor](img/Week_6/M_Servo_2-cropped.jpg)

[Image Source](https://www.smart-prototyping.com/image/cache/data/2_components/actuators/Servo/101820%20MG995/M_Servo_2-750x750.jpg)

Remember PWM that we used to change brightness of LEDs? PWM is also used to control servos, with a certain duty cycle corresponding to a certain position.

![Servo PWM](img/Week_6/miymakers-servo-pwm.png)
[Image Source](https://miymakers.files.wordpress.com/2017/04/miymakers-servo-pwm.png?w=800&h=414)

There is an Arduino library available for controlling servos, called "Servo". To use this library, add `#include <Servo.h>` at the top of your sketch.

## In-class Activities

### [ Activity 1 ] IR Demo

1. Before we start with the IR sensor, you'll need to install the IR Remote library. In the IDE, go to **Sketch** > **Include Library** > **Manage Libraries...**

    ![Install IR Remote 1](img/Week_6/Install_IR_Remote_Lib_01.png)

2. In the "Filter your search..." text box, type "IRremote" (case-insensitive) and click the **Install** button for the IRremote library by shirriff.
    
    ![Install IR Remote 2](img/Week_6/Install_IR_Remote_Lib_02.png)

3. Once the install completes, click the **Close** button on the bottom-right of the Library Manager window to return to the IDE.

4. Go to File > Examples > IRremote > IRrecvDemo

    ![IR Demo 1](img/Week_6/IR_Demo_01.png)

5. Look at the first few lines of the code. In the comment, it says to connect the IR detector to the input RECV_PIN. Below are 2 definitions of the IR_RECEIVE_PIN number. This library is written to work on many different hardware devices - the first `#if defined(ESP32)` checks if you are on a board with an ESP32 microcontroller (processor). We are not, so we will use pin 11.

    FYI: `#if`, `#else`, and `#endif` are known as "preprocessor directives" and tells the compiler to evaluate the expressions before it tries to compile the code. In this case, it uses a if-statement to select which `int IR_RECEIVE_PIN = ` line to use.

        /*
        * IRremote: IRrecvDemo - demonstrates receiving IR codes with IRrecv
        * An IR detector/demodulator must be connected to the input RECV_PIN.
        * Version 0.1 July, 2009
        * Copyright 2009 Ken Shirriff
        * http://arcfn.com
        */

        #include <IRremote.h>

        #if defined(ESP32)
        int IR_RECEIVE_PIN = 15;
        #else
        int IR_RECEIVE_PIN = 11;
        #endif

6. Now let's build the hardware. You will need:

    - Qty 1 - short black wire
    - Qty 1 - short red wire
    - Qty 1 - short green wire
    - Qty 1 - VS1838 IR receiver (comes on a small circuit card, silver body)
    - Qty 1 - remote control

    Assemble the following circuit, using the **breadboard** to make the electrical connections:

    ![IR_Receiver_Wiring.png](img/Week_6/IR_Receiver_Wiring.png)

    Source: <a href="https://gitlab.com/benwang/arduinoClass/-/raw/master/Course_Materials/Week_6/2.12_IR_Receiver_Module.pdf" target="_blank">Elegoo IR receiver documentation</a>

7. Upload the program. Open the serial monitor. Set baud rate to 115200 (if not already). Pull the tab out of the remote to activate the battery. Point your remote control at it and press any button!