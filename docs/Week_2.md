# Week 2

<!-- JS for copy code to clipboard button -->
<script src="../js/clipboard.min.js"></script>

<p style="color: red; ">July 29, 2020</p>

## Meeting Info

This week **only**, we will meet on **Wednesday at 7:30pm CT/8:30pm ET** and last for approximately 1.5 to 2 hours. <b>Link to the class <a href="https://wustl-hipaa.zoom.us/j/93348244242" target="_blank">Zoom meeting</a></b>.

## Objectives

**Hardware:**

- Breadboards
- Circuits
- LEDs (stands for "Light Emitting Diode")
- Introduce the rest of the components and see if there's ones with particular interest

**Software:**

- Digital vs Analog
- analogWrite()
- Logic & logical operators
    - if, then, else
    - and, or, not

## During Class

**Week 1 Homework**

Example solution to Week 1 homework available <a href="https://gitlab.com/benwang/arduinoClass/-/blob/master/Course_Materials/Week_1/Week_1_Homework_Solution/Week_1_Homework_Solution.ino" target="_blank">here</a>.

**Course Material:**

- Week 2 slides available <a href="https://gitlab.com/benwang/arduinoClass/-/raw/master/Course_Materials/Week_2/Week_2_Slides.pdf?inline=true" target="_blank">here</a>
- Full Week 2 course materials <a href="https://gitlab.com/benwang/arduinoClass/-/tree/master/Course_Materials/Week_2" target="_blank">here</a>

## In-class Examples

### [ Activity 0 ] Organize our Sketches

1. In your Arduino folder from last week (on Windows, probably under `My Documents\Arduino\`. Mac or Linux, probably under `~/arduino/`), create a folder titled `Week_1` and move all of the projects from last week into there.
2. Now create a `Week_2` folder for this week. We'll save all sketches into here this time.
3. Let's start with a more advanced serial example (and talk math). This is an example that takes a math expression and computes it first as integers, second as floating point.

### [ Activity 1 ] Basic LED

We will start by repeating Blink from last week, except with an external LED.

1. From your kit, grab the following:
    - Qty 1 - green LED
    - Qty 1 - 330 Ohm resistor
    - Qty 1 - short black wire
    - Qty 1 - short green wire
    - Qty 1 - large breadboard
2. Make sure your Arduino is **NOT** plugged into your computer (it's powered off)
3. Perform the following, so that your circuit looks like the drawing below.

    ![Single LED wiring](img/Week_2/Single_LED_Wiring_bb.png)

    1. Connect the black wire from any pin marked "GND" on your Arduino to the blue, "-" rail on the breadboard.
    2. Plug in the LED vertically, such that:
        1. the negative lead of the LED (the *shorter* pin with the *flat* side on the LED) is plugged into the blue "-" rail on the breadboard.
        2. the positive lead of the LED (the longer pin, rounded side of the LED) is plugged into any hole in the middle/white section.
    3. Take the 330 Ohm resistor and bend it such that it makes a C shape, as shown here:

        ![330 Ohm Resistor](img/Week_2/330_Ohm_Resistor.jpg)

    4. Plug in the 330 Ohm resistor horizontally, such that:
        1. One lead (doesn't matter which one) is plugged into the same vertical column as the positive lead of the LED from step 3.2.2, above (negative lead)
        2. The other lead is plugged into any hole of any new row. (positive lead)
    5. Plug in one lead of the green wire so it is on the same row as the positive lead of the resistor (step 3.4.2).

4. We will first test that you wired this properly by plugging the unconnected end of the green wire to the 5V output on the Arduino.
5. Double check that your wiring matches the drawing in step 3, except that the green wire should be plugged into digital pin 13.
6. Plug in the Arduino to your computer via USB. You should see the LED light up. If not, unplug immediately and check that you followed the above steps or ask for help.

7. If the LED lit up, the test was successful. Good work!
8. Now unplug your USB again. Move the green wire from the 5V output on the Arduino to digital pin 13.
9. Plug in your Arduino via USB again and upload the Blink example (see [Week 1 Activity 1](Week_1.md#activity-1-blink-example) if you don't remember how).

You should now see the LED blinking in sync with the built-in LED on the Arduino!


### [ Activity 2 ] Dimming a LED

Now we'll add some logic so that you can increase or decrease the brightness of the LED. Let's implement a relaxing pulsing or "breathing" effect.

1. Move the wire you plugged into pin 13 in Activity 1 to any pin with a tilde (~) in front of it. For example, pin 3.
2. Go to **File** > **New** to create a new sketch. Change the code to the following:

    <button class="btn" id="copy-button" data-clipboard-target="#targetActivity2">Click to Copy Code Below</button>
    <pre xml:space="preserve" id="targetActivity2">

        // Set this to the pin your LED is plugged into
        const int LED_PIN = 3;
        int brightness = 1;
        int brightnessStep = 1;

        void setup() {
          // Initialize LED pin
          pinMode(LED_PIN, OUTPUT);
        }

        void loop() {
          // Write the brightness
          analogWrite(LED_PIN, brightness);

          // Check if we're at max or min brightness
          // "||" is the syntax for "or" in conditional statements
          if (brightness == 0 || brightness == 255) {
            // Flip the sign on the step (-1 becomes 1, 1 becomes -1)
            // "*=" is shorthand for "brightnessSte = brightnessStep * -1;"
            brightnessStep *= -1;
          }

          // Increment or decrement the brightness accordingly
          // "+=" is shorthand for "brightness = brightness + brightnessStep;"
          brightness += brightnessStep;

          // Refresh period in milliseconds (ms)
          delay(10);
        }
    </pre>
    <!-- Need this line here to workd -->
    <script>new ClipboardJS('.btn');</script>

3. Save this in the `Week_2` folder as "Activity_02-Breathing_LED"
4. Upload and run!
5. Can you make it strobe faster?
6. <p style="color: darkblue;">**[Challenge]** Now add serial to the program so that you can command a brightness by typing in an integer between 0 and 255. You should be able to copy some of the code from [Week 1 Activity 5](Week_1.md#activity-5-Serial-03-double-it)</p>

#### Explaination

Previously, we have only been able to control the on or off of the LED. On or off in computing is known as "digital".

But what happens if you want something in between? Instead of 0 Volts or 5 Volts, we want something in the middle. This is known as "analog".

Have you heard of a "digital clock" or an "analog clock"?

However, the Arduino does not have the capability to output a true analog signal. Instead, it uses something known as PWM, or Pulse Width Modulation. You have already done PWM in Week 1, when we modified Blink so that it blinked so fast you could no longer tell it was blinking. The effect was that it appeared "dim". This is the most common ways we dim LEDs. Have you ever noticed how some lights appear to strobe or turn on and off when you record video on your phone?

`analogWrite()` on the Arduino uses PWM to create analog signals. Next, we will use this to create any color the human eye can see.

### [ Activity 3 ] Multicolor LED

Have you seen the smart light bulbs like the Philips Hue? Let's make one!

1. Disassemble the circuit from Activity 1 and 2. Grab the following parts:
    - Qty 1 - 4-pin, multicolor LED
    - Qty 3 - 330 Ohm resistor
    - Qty 2 - short black wire
    - Qty 1 - short orange wire
    - Qty 1 - short green wire
    - Qty 1 - short blue wire
    - Qty 1 - large breadboard
2. Make sure your Arduino is **NOT** plugged into your computer (it's powered off)
3. Perform the following, so that your circuit looks like the drawing below.

    ![Multicolor LED wiring](img/Week_2/Multicolor_LED_Wiring_bb.png)

    1. Connect the black wire from any pin marked "GND" on your Arduino to the blue, "-" rail on the breadboard.
    2. Plug in the LED horizontally, such that the flat side of the LED is on the left. This should correspond to the pin lengths being medium, long, medium, short from left to right. Here, I use row E, columns 20 through 23

        Note: The pins are red+, common- (to ground), green+, blue+

    3. Take the three 330 Ohm resistors and bend it such that it makes a C shape, as shown here:



    4. Plug in a 330 Ohm resistor horizontally, such that one lead is in D16 and the other is in D20 for the red component of the LED.
    5. Plug in a 330 Ohm resistor horizontally, such that one lead is in C18 and the other is in C22 for the green component of the LED.
    6. Plug in a 330 Ohm resistor horizontally, such that one lead is in B19 and the other is in B23 for the blue component of the LED.
    7. Plug in one lead of the orange wire so it is on the same row as the positive lead of the red component's resistor (A16) and the other end into the "~6" pin on the Arduino.
    8. Plug in one lead of the green wire so it is on the same row as the positive lead of the green component's resistor (A18) and the other end into the "~5" pin on the Arduino.
    9. Plug in one lead of the blue wire so it is on the same row as the positive lead of the blue component's resistor (A19) and the other end into the "~3" pin on the Arduino.

4. Create a new sketch (**File** > **New**) and paste in the following:

    <button class="btn" id="copy-button" data-clipboard-target="#targetActivity3">Click to Copy Code Below</button>
    <pre xml:space="preserve" id="targetActivity3">

        // Pin definitions here
        // These MUST be PWM pins. For Arduino UNO, choose from: 3, 5, 6, 9, 10, 11
        const int RED_PIN = 6;
        const int GREEN_PIN = 5;
        const int BLUE_PIN = 3;

        // This function prints the valid color options
        void printHelpText() {
            Serial.println("Valid colors are: red, yellow, green, aqua, blue, violet, white, off");
            Serial.println("Please type a color and hit enter.");
        }

        void setup() {
            // Initialize the 3 digital output pins for red, green, and blue
            pinMode(RED_PIN, OUTPUT);
            pinMode(GREEN_PIN, OUTPUT);
            pinMode(BLUE_PIN, OUTPUT);

            // Initialize serial
            Serial.begin(9600);

            // Initial prompt
            printHelpText();
        }

        void loop() {
            // Good practice to declare and initialize variables at beginning of function
            String inputString;
            int redValue = 0;
            int greenValue = 0;
            int blueValue = 0;

            // Only process if there is data available on serial
            if (Serial.available()) {   
                // Read the characters available via serial as a string object
                inputString = Serial.readString();

                // Get rid of any extraneous newline characters (like if serial monitor is set to Newline mode)
                inputString.replace("\n", "");

                // Check for preprogrammed color using if-statements
                if (inputString == "red") {
                    redValue = 255;
                    greenValue = 0;
                    blueValue = 0;
                } else if (inputString == "yellow") {
                    redValue = 255;
                    greenValue = 255;
                    blueValue = 0;
                } else if (inputString == "green") {
                    redValue = 0;
                    greenValue = 255;
                    blueValue = 0;
                } else if (inputString == "aqua") {
                    redValue = 0;
                    greenValue = 255;
                    blueValue = 255;
                } else if (inputString == "blue") {
                    redValue = 0;
                    greenValue = 0;
                    blueValue = 255;
                } else if (inputString == "violet") {
                    redValue = 255;
                    greenValue = 0;
                    blueValue = 255;
                } else if (inputString == "white") {
                    redValue = 255;
                    greenValue = 255;
                    blueValue = 255;
                } else if (inputString == "off") {
                    redValue = 0;
                    greenValue = 0;
                    blueValue = 0;
                } else {
                    Serial.println("ERROR: Unrecognized color \'" + inputString + "\'");
                    printHelpText();
                }

                // Set the color
                analogWrite(RED_PIN, redValue);
                analogWrite(GREEN_PIN, greenValue);
                analogWrite(BLUE_PIN, blueValue);
            }
        }
    </pre>


5. Save As "Activity_03-Multicolor_LED". Upload and run
6. Open a serial monitor and follow the instructions that are printed over serial!

#### Notes

- We use "==" for comparisons ("=" is already taken as the assignment operator!)
- if-then-else is used for logic
- Because the color options (help text) is run multiple times, it's better to define it as a function so that if things change, it only has to be updated once. Hence the function `printHelpText()` defined at the beginning.
- Remember the extra "0" that would show up in Week 1's Double-it activity? The extra new line character is solved with the line `inputString.replace("\n", "");` here, which replaces all occurances of the newline character (`\n`) with nothing, i.e., deletes it.

### [ Activity 4 ] Multicolor LED - Define a color

Now define a new color - one of your own. You can use this to help you pick: <a href="https://rgbcolorcode.com/color/yellow" target="_blank">https://rgbcolorcode.com/color/yellow</a>.

1. Add it to the chain of if-then-else statements. Make sure you use the RGB values that are 0-255, as shown here:

    ![RGB picker](img/Week_2/RGB_color_picker_value.png)

2. Update the color options in `printHelpText()` with your new color.
3. Try it out!
4. Remember to save your code before you move on!
5. <p style="color: darkblue;">**[Challenge]** One more thing - right now our program is case-sensitive. You must type "red" rather than "RED" or "Red". Let's make it more user friendly. The String class has a function called <a href="https://www.arduino.cc/reference/en/language/variables/data-types/string/functions/tolowercase/" target="_blank">`toLowerCase()`</a> that will modify an existring string so all characters are lowercase. Add the following line directly after we remove the newline character (`inputString.replace("\n", "");`)</p>

        // Convert all characters to lowercase ones
        inputString.toLowerCase();

<details>
    <summary><b>Example solution</b></summary>

```c++
// Pin definitions here
// These MUST be PWM pins. For Arduino UNO, choose from: 3, 5, 6, 9, 10, 11
const int RED_PIN = 6;
const int GREEN_PIN = 5;
const int BLUE_PIN = 3;

// This function prints the valid color options
void printHelpText() {
  Serial.println("Valid colors are: red, orange, yellow, green, aqua, blue, violet, white, off");
  Serial.println("Please type a color and hit enter.");
}

void setup() {
  // Initialize the 3 digital output pins for red, green, and blue
  pinMode(RED_PIN, OUTPUT);
  pinMode(GREEN_PIN, OUTPUT);
  pinMode(BLUE_PIN, OUTPUT);

  // Initialize serial
  Serial.begin(9600);

  // Initial prompt
  printHelpText();
}

void loop() {
  // Good practice to declare and initialize variables at beginning of function
  String inputString;
  int redValue = 0;
  int greenValue = 0;
  int blueValue = 0;

  // Only process if there is data available on serial
  if (Serial.available()) {   
    // Read the characters available via serial as a string object
    inputString = Serial.readString();

    // Get rid of any extraneous newline characters (like if serial monitor is set to Newline mode)
    inputString.replace("\n", "");

    // Convert all characters to lowercase ones
    inputString.toLowerCase();

    // Check for preprogrammed color using if-statements
    if (inputString == "red") {
        redValue = 255;
        greenValue = 0;
        blueValue = 0;
    } else if (inputString == "yellow") {
        redValue = 255;
        greenValue = 255;
        blueValue = 0;
    } else if (inputString == "green") {
        redValue = 0;
        greenValue = 255;
        blueValue = 0;
    } else if (inputString == "aqua") {
        redValue = 0;
        greenValue = 255;
        blueValue = 255;
    } else if (inputString == "blue") {
        redValue = 0;
        greenValue = 0;
        blueValue = 255;
    } else if (inputString == "violet") {
        redValue = 255;
        greenValue = 0;
        blueValue = 255;
    } else if (inputString == "white") {
        redValue = 255;
        greenValue = 255;
        blueValue = 255;
    } else if (inputString == "orange") {
        redValue = 255;
        greenValue = 128;
        blueValue = 0;
    } else if (inputString == "off") {
        redValue = 0;
        greenValue = 0;
        blueValue = 0;
    } else {
        Serial.println("ERROR: Unrecognized color \'" + inputString + "\'");
        printHelpText();
    }

    // Set the color
    analogWrite(RED_PIN, redValue);
    analogWrite(GREEN_PIN, greenValue);
    analogWrite(BLUE_PIN, blueValue);
  }
}

```
</details>

<br>

### [Challenge][ Activity 5 ] Multicolor LED - Custom RGB

Now try this one, which lets you customize your colors. It's similar to Activity 4 above, except that we changed the parsing logic. Upload, open Serial Monitor, and follow the instructions there.

<button class="btn" id="copy-button" data-clipboard-target="#targetActivity5">Click to Copy Code Below</button>
<pre xml:space="preserve" id="targetActivity5">

```c++
// Pin definitions here
// These MUST be PWM pins. For Arduino UNO, choose from: 3, 5, 6, 9, 10, 11
const int RED_PIN = 6;
const int GREEN_PIN = 5;
const int BLUE_PIN = 3;

// This function prints the valid color options
void printHelpText() {
  Serial.println("Valid colors are: red, yellow, green, aqua, blue, violet, white, off");
  Serial.println("You can also specify any color with RGB values with: rgb 255 128 0");
  Serial.println("Please type a color and hit enter.");
}

void setup() {
  // Initialize the 3 digital output pins for red, green, and blue
  pinMode(RED_PIN, OUTPUT);
  pinMode(GREEN_PIN, OUTPUT);
  pinMode(BLUE_PIN, OUTPUT);

  // Initialize serial
  Serial.begin(9600);

  // Initial prompt
  printHelpText();
}

void loop() {
  // Good practice to declare and initialize variables at beginning of function
  String inputString;
  int redValue = 0;
  int greenValue = 0;
  int blueValue = 0;

  // Only process if there is data available on serial
  if (Serial.available()) {   
    // Read the characters available via serial as a string object
    inputString = Serial.readString();

    // Get rid of any extraneous newline characters (like if serial monitor is set to Newline mode)
    inputString.replace("\n", "");

    // Convert all characters to lowercase ones
    inputString.toLowerCase();

    // Check for preprogrammed color using if-statements
    if (inputString == "red") {
        redValue = 255;
        greenValue = 0;
        blueValue = 0;
    } else if (inputString == "yellow") {
        redValue = 255;
        greenValue = 255;
        blueValue = 0;
    } else if (inputString == "green") {
        redValue = 0;
        greenValue = 255;
        blueValue = 0;
    } else if (inputString == "aqua") {
        redValue = 0;
        greenValue = 255;
        blueValue = 255;
    } else if (inputString == "blue") {
        redValue = 0;
        greenValue = 0;
        blueValue = 255;
    } else if (inputString == "violet") {
        redValue = 255;
        greenValue = 0;
        blueValue = 255;
    } else if (inputString == "white") {
        redValue = 255;
        greenValue = 255;
        blueValue = 255;
    } else if (inputString == "off") {
        redValue = 0;
        greenValue = 0;
        blueValue = 0;
    } else if (inputString.startsWith("rgb")) {
        // The 4th character should be a space, we'll start looking at the 5th character (index of 4 because C++ indices start at 0)
        int numStartIndex = 4;
        int numEndIndex = inputString.indexOf(" ", numStartIndex);
        String valueString = inputString.substring(numStartIndex, numEndIndex);
        redValue = valueString.toInt();

        numStartIndex = numEndIndex + 1;
        numEndIndex = inputString.indexOf(" ", numStartIndex);
        valueString = inputString.substring(numStartIndex, numEndIndex);
        greenValue = valueString.toInt();

        numStartIndex = numEndIndex + 1;
        numEndIndex = inputString.indexOf(" ", numStartIndex);
        valueString = inputString.substring(numStartIndex, numEndIndex);
        blueValue = valueString.toInt();

    } else {
        Serial.println("ERROR: Unrecognized color \'" + inputString + "\'");
        printHelpText();
    }

    // Print the RGB being set
    Serial.println("Setting color to R:" + String(redValue) + " G:" + String(greenValue) + " B:" + String(blueValue));

    // Set the color
    analogWrite(RED_PIN, redValue);
    analogWrite(GREEN_PIN, greenValue);
    analogWrite(BLUE_PIN, blueValue);
  }
}

```
</pre>

However, you'll notice that if you give it a number outside of the valid range (0 through 255, inclusive), there's no error message. Go and add error checks so that if an out-of-range value is entered, an error is printed.

<details>
    <summary><b>Click here for a hint</b></summary>
```c++
if (value1 > 255 || value1 < 0 || value2 > 255 || ...) {
    // Print error
    Serial.println("ERROR: RGB values must be in the range 0 through 255, inclusive!");
} else {
    // Set color normally
    ...
}
```
</details>
<br>

<details>
  <summary><b>Example Solution</b></summary>

```c++
// Pin definitions here
// These MUST be PWM pins. For Arduino UNO, choose from: 3, 5, 6, 9, 10, 11
const int RED_PIN = 6;
const int GREEN_PIN = 5;
const int BLUE_PIN = 3;

// This function prints the valid color options
void printHelpText() {
  Serial.println("Valid colors are: red, yellow, green, aqua, blue, violet, white, off");
  Serial.println("You can also specify any color with RGB values with: rgb 255 128 0");
  Serial.println("Please type a color and hit enter.");
}

void setup() {
  // Initialize the 3 digital output pins for red, green, and blue
  pinMode(RED_PIN, OUTPUT);
  pinMode(GREEN_PIN, OUTPUT);
  pinMode(BLUE_PIN, OUTPUT);

  // Initialize serial
  Serial.begin(9600);

  // Initial prompt
  printHelpText();
}

void loop() {
  // Good practice to declare and initialize variables at beginning of function
  String inputString;
  int redValue = 0;
  int greenValue = 0;
  int blueValue = 0;

  // Only process if there is data available on serial
  if (Serial.available()) {   
    // Read the characters available via serial as a string object
    inputString = Serial.readString();

    // Get rid of any extraneous newline characters (like if serial monitor is set to Newline mode)
    inputString.replace("\n", "");

    // Convert all characters to lowercase ones
    inputString.toLowerCase();

    // Check for preprogrammed color using if-statements
    if (inputString == "red") {
        redValue = 255;
        greenValue = 0;
        blueValue = 0;
    } else if (inputString == "yellow") {
        redValue = 255;
        greenValue = 255;
        blueValue = 0;
    } else if (inputString == "green") {
        redValue = 0;
        greenValue = 255;
        blueValue = 0;
    } else if (inputString == "aqua") {
        redValue = 0;
        greenValue = 255;
        blueValue = 255;
    } else if (inputString == "blue") {
        redValue = 0;
        greenValue = 0;
        blueValue = 255;
    } else if (inputString == "violet") {
        redValue = 255;
        greenValue = 0;
        blueValue = 255;
    } else if (inputString == "white") {
        redValue = 255;
        greenValue = 255;
        blueValue = 255;
    } else if (inputString == "off") {
        redValue = 0;
        greenValue = 0;
        blueValue = 0;
    } else if (inputString.startsWith("rgb")) {
        // The 4th character should be a space, we'll start looking at the 5th character (index of 4 because C++ indices start at 0)
        int numStartIndex = 4;
        int numEndIndex = inputString.indexOf(" ", numStartIndex);
        String valueString = inputString.substring(numStartIndex, numEndIndex);
        redValue = valueString.toInt();

        numStartIndex = numEndIndex + 1;
        numEndIndex = inputString.indexOf(" ", numStartIndex);
        valueString = inputString.substring(numStartIndex, numEndIndex);
        greenValue = valueString.toInt();

        numStartIndex = numEndIndex + 1;
        numEndIndex = inputString.indexOf(" ", numStartIndex);
        valueString = inputString.substring(numStartIndex, numEndIndex);
        blueValue = valueString.toInt();

    } else {
        Serial.println("ERROR: Unrecognized color \'" + inputString + "\'");
        printHelpText();
    }

    // Check that RGB values are in-range
    if (redValue > 255 || redValue < 0 || blueValue > 255 || blueValue < 0 || greenValue > 255 || greenValue  < 0) {
      Serial.println("ERROR: RGB values must be in the range 0 through 255, inclusive!");
      printHelpText();
    } else {
      // Print the RGB being set
      Serial.println("Setting color to R:" + String(redValue) + " G:" + String(greenValue) + " B:" + String(blueValue));
  
      // Set the color
      analogWrite(RED_PIN, redValue);
      analogWrite(GREEN_PIN, greenValue);
      analogWrite(BLUE_PIN, blueValue);
    }
  }
}
```
</details>
<br>

================================ **Stop here** ================================

<br>

### [ Actvitiy 6 ] Analog Input (Joystick)

You can leave the circuit from Activities 3-5 (you'll need it in Activity 7). We will add on the joystick at this time.

1. Grab the following:
    - Qty 1 - Joystick circuit card
    - Qty 1 - Joystick cap
    - Qty 1 - Medium yellow wire
    - Qty 1 - Medium white wire
    - Qty 1 - Medium red wire
    - Qty 1 - Short black wire
2. Wire up the joystick like in the following diagram:

    ![Joystick Activity Diagram](img/Week_2/Multicolor_LED_Joystick_Wiring_bb.png)

4. Create a new sketch and paste in the following:

    <button class="btn" id="copy-button" data-clipboard-target="#targetActivity6">Click to Copy Code Below</button>
    <pre xml:space="preserve" id="targetActivity6">

        // Joystick pins
        const int JOYSTICK_VX_PIN = A0;
        const int JOYSTICK_VY_PIN = A1;

        void setup() {
            // Initialize serial
            Serial.begin(9600);
        }

        void loop() {
            // Read joystick X and Y analog values
            int joystickXValue = analogRead(JOYSTICK_VX_PIN);
            int joystickYValue = analogRead(JOYSTICK_VY_PIN);

            Serial.println("Joystick position: (" + String(joystickXValue) + ", " + String(joystickYValue) + ")  ");

            // Slow down
            delay(500);
        }
    </pre>

5. Save as "Activity_06-Joystick" in `Week_2`.
6. Compile and upload. Open the serial monitor and move the stick around.
7. <p style="color: darkblue;">**[Challenge]** Now modify the code so that the output is -1.0 to 1.0 (of data type `double`) instead of 0 to 1023 (of data type `int`).</p>

#### Explanation

The joystick has 2 axes, an x-axis and a y-axis. Both axes have a variable resistor (a.k.a., potentiometer), which changes its resistance as you move the sick. By applying 5 Volts across this variable resistor, a variable, "analog" voltage is output by the joystick varying between 0V and 5V. The Arduino as onboard "analog-to-digital converters" (ADCs) that take an analog voltage and turn it into a number between 0 and 1023, which is what's printed over serial.

#### Questions

1. What should the ADCs' values be when the joystick is untouched?
2. Is that what you see? Why might this happen?



### [ Activity 7 ] Joysick Multicolor LED

Now we'll combine both the multicolor LED with joystick controls. Make a new sketch, save as "Activity_07-Joystick_Multicolor_LED". Run the following program

<button class="btn" id="copy-button" data-clipboard-target="#targetActivity7">Click to Copy Code Below</button>
<pre xml:space="preserve" id="targetActivity7">

```c++
// Include math library for trig functions
#include <math.h>


// Pin definitions here
// These MUST be PWM pins. For Arduino UNO, choose from: 3, 5, 6, 9, 10, 11
const int RED_PIN = 6;
const int GREEN_PIN = 5;
const int BLUE_PIN = 3;

// Joystick pins
const int JOYSTICK_VX_PIN = A0;
const int JOYSTICK_VY_PIN = A1;

// Declaring RGB as global variables now so they can be accessed by colorWheelTransform()
int redValue = 0;
int greenValue = 0;
int blueValue = 0;

// Declare this before we use it
inline double normalizeAngle(double angle);

// Angle definitions for color wheel. These are the angles at which
// each color is off
const double RED_OFF_ANGLE = normalizeAngle(- M_PI / 2);
const double GREEN_OFF_ANGLE = normalizeAngle(RED_OFF_ANGLE + (2 * M_PI / 3));
const double BLUE_OFF_ANGLE = normalizeAngle(RED_OFF_ANGLE - (2 * M_PI / 3));

// Magnitude of joystick "deadzone"
const double JOYSTICK_DEADZONE_MAG = 0.07;

// ********************************************
// ************* HELPER FUNCTIONS *************
// ********************************************

// Function to normalize an angle to be between -pi and +pi
inline double normalizeAngle(double angle) {
  while (angle > M_PI) {
    angle -= 2 * M_PI;
  }
  while (angle < - M_PI) {
    angle += 2 * M_PI;
  }

  return angle;
}

// Given a joystick X,Y, convert it to an RGB value
void colorWheelTransform(int x, int y) {
  // Normalize and convert to floating point (-1.0 to 1.0, instead of 0 to 1023)
  double normX = (x - 512) / 512.0;
  double normY = (y - 512) / 512.0;
  
  Serial.print("norm: (" + String(normX) + ", " + String(normY) + ")  ");

  // Now compute the angle using trigonometry
  double angle = atan2(normY, normX);
  double magnitude = min(sqrt(normX * normX + normY * normY), 1.0);

  Serial.print("Angle: " + String(angle * 180 / M_PI) + " Mag: " + String(magnitude) + "  ");

  // Add a deadzone so that noise when stick is untouched doesn't jump around
  if (magnitude > JOYSTICK_DEADZONE_MAG) {
    // Serial.print(" R_OFF:" + String(RED_OFF_ANGLE) + " G_OFF:" + String(GREEN_OFF_ANGLE) + " B_OFF:" + String(BLUE_OFF_ANGLE) + "  ");
    
    // Blend the 3 components based on angle and magnitude
    redValue = 255 * magnitude * abs(normalizeAngle(RED_OFF_ANGLE - angle)) / M_PI;
    greenValue = 255 *  magnitude * abs(normalizeAngle(GREEN_OFF_ANGLE - angle)) / M_PI;
    blueValue = 255 * magnitude * abs(normalizeAngle(BLUE_OFF_ANGLE - angle)) / M_PI;
  } else {
    redValue = 0;
    greenValue = 0;
    blueValue = 0;
  }
}


// ********************************************
// ******************* MAIN *******************
// ********************************************

void setup() {
  // Initialize the 3 digital output pins for red, green, and blue
  pinMode(RED_PIN, OUTPUT);
  pinMode(GREEN_PIN, OUTPUT);
  pinMode(BLUE_PIN, OUTPUT);

  // Initialize serial
  Serial.begin(9600);
}

void loop() {
  // Read joystick X and Y analog values
  int joystickXValue = analogRead(JOYSTICK_VX_PIN);
  int joystickYValue = analogRead(JOYSTICK_VY_PIN);

  Serial.print("Joystick position: (" + String(joystickXValue) + ", " + String(joystickYValue) + ")  ");

  // Convert X,Y value into RGB
  colorWheelTransform(joystickXValue, joystickYValue);

  // Print the RGB being set
  Serial.println("Color: (R:" + String(redValue) + ", G:" + String(greenValue) + ", B:" + String(blueValue) + ")");

  // Set the color
  analogWrite(RED_PIN, redValue);
  analogWrite(GREEN_PIN, greenValue);
  analogWrite(BLUE_PIN, blueValue);

  // Slow down
  delay(500);
}
```
</pre>

#### Questions

What do you think? Answer the following questions:

1. Does it do what you expect?
2. Why is there a "deadzone" for the joystick? What might be the issue if it wasn't there?
3. Does it feel responsive?

## Homework

1. Look through the components in your Arduino kit and let me know if there are ones you are particularly interested in learning or using
2. After completing Activity 7, answer the questions at the end and email me your answers.
3. You may have noticed that, while the Activity 7 code works for controlling the LED color and brightness with the joystick, the performance is not very good. The user experience is laggy and unpleasant. Fix this by:
    1. Commenting out unecessary debug print (each character printed takes 1/9600 of a second - this adds up when printing 10s of characters every loop!)
    2. Decreasing the number of milliseconds used in the `delay()` function. to increase the refresh rate