# Syllabus

## Course Hardware

You will need the following for this class:

- [Arduino Uno kit](https://www.amazon.com/gp/product/B01D8KOZF4)
- PC with available USB port

## Class Structure

Each class will be broken into four parts:

- Recap
- Hardware
- Software
- Activities

In the hardware section, we'll introduce a new piece of hardware that we'll be working with. We'll cover the fundamentals, the physics (if applicable), and the electrical interface.

We'll then talk about software. This includes (1) new software concepts involving C++ or the Arduino API, and (2) the code required to use the new hardware.

Finally, we'll do some excercises using what we talk about to build and test something.

There will be a homework assignment (possibly with a more advanced, challenge option) using what we learned.

## Schedule & Meeting Time

We will meet on **Thursdays at 7:30pm CT/8:30pm ET** and last for approximately 1.5 to 2 hours. <b>Link to the class <a href="https://wustl-hipaa.zoom.us/j/93348244242" target="_blank">Zoom meeting</a></b>.

|  Week  |  Date  |  Topic  |
|--|--|--|
| 1 | 7/23/2020 | Arduino & Class Intro |
| 2 | <p style="color: red; margin-bottom: 0px;">7/29/2020</p> | LEDs |
| 3 | 8/6/2020  | Joystick + Buzzer |
| 4 | 8/13/2020 | Loops & Logic 1 |
| 5 | <p style="color: red; margin-bottom: 0px;">8/27/2020</p> | Logic & Loops 2 |
| 6 | 9/3/2020  | IR remote |
| 7 | 9/10/2020 | Servo |
| 8 | 9/17/2020 | Temp/Humidity sensor + LCD display |

<br>

## Terms

| Term | Definition |
|--|--|
| API | Application Programming Interface. The definition of the interface into a software library or tool so that a programmer can use something someone else has created. Example is the Arduino API, which provides functions such as Serial.println() or digitalWrite() to access the hardware |

<br>