# Week 7

<!-- JS for copy code to clipboard button -->
<script src="../js/clipboard.min.js"></script>

September 10, 2020

## Meeting Info

We will meet on **Thursdays at 7:30pm CT/8:30pm ET** and last for approximately 1.5 to 2 hours. <b>Link to the class <a href="https://wustl-hipaa.zoom.us/j/93348244242" target="_blank">Zoom meeting</a></b>.

## Objectives

**Software:**

- IR library
- Servo library

**Hardware:**

- IR Receiver
- IR Remote
- Servo motor

## During Class

**Additional Resources**

Additional resources (tutorials, example code, documentation) is available for the kit at Elegoo's <a href="https://www.elegoo.com/tutorial/Elegoo%20Super%20Starter%20Kit%20for%20UNO%20V2.0.2020.5.13.zip" target="_blank">website</a>.

### Infrared & Remote Controls

Have you ever held a camera (eg., smartphone camera) at a TV remote and pressed the button? *Note that your camera CANNOT have an infrared filter built-in. Try the front-facing camera if the rear one doesn't work.* You should be able see a quick flash of light when you click any button.

The flash of light is a digital signal. The single flash is actually many 1s and 0s, represenging a number. The TV and remote have an established "code" for each command. This is how universal remotes are able to communicate with a huge range of TVs, although you have to "program" the remote by telling it which set of codes to use.

Up close, it might look something like this:

![Oscilloscope image](img/Week_6/light_sonycodepulses.jpg)

Source: Adafruit

Adafruit's website has more information: <a href="https://learn.adafruit.com/ir-sensor/ir-remote-signals" target="_blank">IR Remote Signals</a>

### Servo Motors

Servos motors or "servos" are motors with position feedback. This allows you to command an angle and the servo will do whatever it takes (within its power) to reach and hold that position. Typical servos have a 180° range of motion.

![Servo Motor](img/Week_6/M_Servo_2-cropped.jpg)

[Image Source](https://www.smart-prototyping.com/image/cache/data/2_components/actuators/Servo/101820%20MG995/M_Servo_2-750x750.jpg)

Remember PWM that we used to change brightness of LEDs? PWM is also used to control servos, with a certain duty cycle corresponding to a certain position.

![Servo PWM](img/Week_6/miymakers-servo-pwm.png)
[Image Source](https://miymakers.files.wordpress.com/2017/04/miymakers-servo-pwm.png?w=800&h=414)

There is an Arduino library available for controlling servos, called "Servo". To use this library, add `#include <Servo.h>` at the top of your sketch.

## In-class Activities

Continued from Week 6:

### [ Activity 2 ] Servo Demo

Materials:

- Servo motor
- Servo horn (the plastic arms that attach to the servo)
- 1 short black wire
- 1 short red wire
- 1 short yellow wire

Procedure:

1. Wire the servo as shown (**leaving the wiring for the IR receiver from Activity 1**):

    ![Servo Wiring](img/Week_6/Servo_Wiring.png)

    Source: <a href="https://gitlab.com/benwang/arduinoClass/-/raw/master/Course_Materials/Week_6/2.8%20_Servo.pdf" target="_blank">Elegoo servo documentation</a>

2. In the Arduino IDE, go to **File** > **Examples** > **Servo** > **Sweep** to open the servo sweep example.
3. Upload the sketch. Is the servo moving?

### [ Activity 3 ] Command the Servo

Now, modify the Servo Sweep example so that it takes an integer command via serial and moves the servo to that position. You can use the Serial and integer parsing logic from [Week 1 Activity 5](Week_1.md#activity-5-serial-03-double-it).



### [ Activity 4 ] Remote-Controlled Servo

Let's combine both the IR receiver/remote with the servo. To keep it simple, we'll have the servo move to 0 degrees when you press "0" and 90 degrees when you press "1".

#### Extracting the codes

First, we'll need to extract the code representing the "0" and "1" buttons. Running the IRrecvDemo, press "0" on the remote and note the code printed in Serial Monitor. Repeat for the "1" button.

#### Program the response

Now let's add a couple if-statements to handle the key presses.

1. Start from the IRrecvDemo example source code.
2. Before the `setup()` function, add the `#include` and global variables required for the servo. These are copied from the Servo Sweep example.

        #include <Servo.h>

        Servo myservo;  // create servo object to control a servo

3. In the `setup()` function, initialize the servo (copy t)

        void setup() {
            // ... Leave the IR initialization code here

            myservo.attach(9);  // attaches the servo on pin 9 to the servo object
        }

4. Modify the loop() function so that after it prints the code received, it checks for the code corresponding to "0" being pressed.

        if (results.value == 0x<code for "0">) {
            Serial.println("Commanding servo to 0 degrees");
            myservo.write(0);
        }

5. Similarly, add an if-statement to check for "1" being pressed and write 90 degrees to the servo
6. Upload and give it a try!

Where could something like this be useful?

## Homework

Now change the implementation from Activity 4 so that pressing the buttons 0-9 moves the servo to angles at 20 degree increments ("0" -> 0 deg, "1" -> 20 deg, ..., "9" -> 180 deg)

If you didn't write this down earlier, the codes for the remote are below:

| Button | Code |
|-|-|
| 0 | FF6897 |
| 1 | FF30CF |
| 2 | FF18E7 |
| 3 | FF7A85 |
| 4 | FF10EF |
| 5 | FF38C7 |
| 6 | FF5AA5 |
| 7 | FF42BD |
| 8 | FF4AB5 |
| 9 | FF52AD |

<br>