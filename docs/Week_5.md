# Week 5

<!-- JS for copy code to clipboard button -->
<script src="../js/clipboard.min.js"></script>

August 27, 2020

## Meeting Info

We will meet on **Thursdays at 7:30pm CT/8:30pm ET** and last for approximately 1.5 to 2 hours. <b>Link to the class <a href="https://wustl-hipaa.zoom.us/j/93348244242" target="_blank">Zoom meeting</a></b>.

## Objectives

**Software:**

- Recap Week 1-3 concepts. Data types, functions, variables, logic (if-then-else)
- Programming basics!
    - if statements
    - while loops
    - for loops

## During Class

**Additional Resources**

Additional resources (tutorials, example code, documentation) is available for the kit at Elegoo's <a href="https://www.elegoo.com/tutorial/Elegoo%20Super%20Starter%20Kit%20for%20UNO%20V2.0.2020.5.13.zip" target="_blank">website</a>.

### Loops

Loops are used for repeating things.

#### While Loop

Repeat until the condition is met. For example:

```c++
int x = 0;
while (x < 10) {
    // Do something here

    x++;
}
```

Note that `x++` is simply shorthand for `x = x + 1`. Another way to write the same statement is `x += 1`.

Another common use of loops is in programs that run indefinitely (such as Arduino or video games!). For example, the loop() function in Arduino can also be implemented as:

```c++
void setup() {
    // Setup tasks here

    while (true) {
        // Loop tasks here
    }
}
```

#### For Loop

A modified while loop that combines the above lines into 1:

```c++
for (int x = 0; x < 10; x++) {
    // Do something here
    // It will repeat 10 times, for example:
    Serial.println(x);
}
```

The above would print:

```
0
1
2
3
4
5
6
7
8
9
```

### Conditional Statements



#### If statements

Used in a situation where you want to execute some code only when a condition is true. The "condition" is what's inside the parentheses. For example:

```c++
if (input == "hello") {
    Serial.println("goodbye");
}
```

#### Comparisons

Now is a good time to review the comparison operators in C++. These are very common across programming languages:

| Operator | Meaning | Explanation |
|-|-|-|
| == | Equality | True if left and right are equal. False otherwise |
| != | Inequality | True only if the left and right are not equal |
| > | Greater than | True if left is greater than right |
| < | Less than | True if left is less than right |
| >= | Greater than or equal to | True if left is greater than or equal to the right |
| <= | Less than or equal to | True if left is less than or equal to the right |

#### Structure

You may have noticed that C++ uses braces `{}` heavily. C++ does not care about whitespace (spaces, new lines, tabs). Your entire program could be written on one line. Braces are used to group code. For example, the entire "then" block of code is wrapped by a set of braces.

Loops and functions also uses braces too.

Note that if only 1 line follows a if-statement or loop, the braces are optional.

For example, the following 3 chunks of code are identical:

```c++
if (x != 5) {
    Serial.println("x is not five");
}
```

```c++
if (x != 5)
    Serial.println("x is not five");
```

```c++
if (x != 5) Serial.println("x is not five");
```

#### If-then-else

You might want to do something else if the condition is not true:

```c++
if (input == "hello") {
    Serial.println("goodbye");
} else {
    Serial.println("Say hi to me!");
}
```

#### Else-if

You can also nest conditions. We used this in Week 2 for defining the RGB color of the LED:

```c++
if (inputString == "red") {
    redValue = 255;
    greenValue = 0;
    blueValue = 0;
} else if (inputString == "yellow") {
    redValue = 255;
    greenValue = 255;
    blueValue = 0;
} else if (inputString == "green") {
    redValue = 0;
    greenValue = 255;
    blueValue = 0;
} else if (inputString == "aqua") {
    redValue = 0;
    greenValue = 255;
    blueValue = 255;
} else {
    // ...
}
```

## In-class Activities

This week, we'll finish up the activities from Week 4.

### [ Activity 3 ] Even or odd?

Make a sketch that:

1. Prompts the user to input a number over serial.
2. Reads the number and prints either "even" or "odd".

Hint: To determine whether a number is even or not, we can rely on division:

**Method 1: Integer division truncates the decimals**

In integer division, the decimals are truncated. For example, `5 / 3` returns 1, even though your calculator will say it's 1.66666667. The 0.66666667 at the end is cut off.

For example, 4 / 2 = 2. Taking the answer and multiplying by 2 should recreate the original value: `(4 / 2) * 2` is  4.

But what if you feed 5 into the above?

<details>
    <summary><b>Is 5 odd?</b></summary>

5 / 2 is 2.5.
<br>
Truncated would be 2.
<br>
2 * 2 is 4.
<br>
Does the original 5 equal the new 4?
</details>
<br>

How might you implement this in code?

<details>
    <summary><b>Example solution</b></summary>

```c++
int x = 5;

if (((x / 2) * 2) == x) {
    Serial.println(String(x) + " is even");
} else {
    Serial.println(String(x) + " is odd");
}
```
</details>
<br>

**Method 2: Modulo operator**

This is the preferred method. The modulo (mod) operator returns the remainder from division. For example, `5 % 3` returns the remainder of 5 / 3, or 2. How can you use this to determine if a number is even or odd?


<details>
    <summary><b>Example solution</b></summary>

```c++
int x = 5;

if (x % 2 == 0) {
    Serial.println(String(x) + " is even");
} else {
    Serial.println(String(x) + " is odd");
}
```
</details>
<br>

## Homework

Make a sketch that plays a number guessing game. When it starts up, it picks a number between 1 and 100. Then it asks you to guess the number. When you type a number and hit enter in the Serial Monitor, it will tell you if you're too low, too high, or guessed it. When you have properly guessed the answer, print a message (and optionally flash the LED for 5 seconds).

To do this, you will need to use:

- a loop
- if-statements
- a function to generate random numbers
- serial
- optionally the LED on pin 13

To generate a random number in Arduino, use the `random(int min, int max)` function, which returns a random integer between `min` (inclusive) and `max` (exclusive). Note the call to `randomSeed()` in `setup()` - this is necessary because computers can't generate random numbers on their own. What's used is called a "pseudorandom" algorithm that generates evenly distributed numbers. Given the same seed, the same sequence of random numbers would be generated every time. This program reads the arbitrary analog voltage on pin A0 and uses that as the seed.

Arduino's example code for using `random()`:

```c++
long randNumber;

void setup() {
  Serial.begin(9600);

  // if analog input pin 0 is unconnected, random analog
  // noise will cause the call to randomSeed() to generate
  // different seed numbers each time the sketch runs.
  // randomSeed() will then shuffle the random function.
  randomSeed(analogRead(0));
}

void loop() {
  // print a random number from 0 to 299
  randNumber = random(300);
  Serial.println(randNumber);

  // print a random number from 10 to 19
  randNumber = random(10, 20);
  Serial.println(randNumber);

  delay(50);
}
```

Reference: [https://www.arduino.cc/reference/en/language/functions/random-numbers/random/](https://www.arduino.cc/reference/en/language/functions/random-numbers/random/)